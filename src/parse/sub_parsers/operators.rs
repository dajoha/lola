//! Parsers for some specific operators, like number conversion operators, boolean negation,...

use super::nom::{
    tag,
    multispace0,
    pair,
    preceded,
    alt,
    map,
};

use crate::context::Container;
use crate::value::Unit;
use crate::eval::{AssignMode, Expr, ExprSpan};
use crate::parse::{Span, Input};

use super::{UParser};
use super::{assignable, optional_unit};
use super::spaced;
use super::to_expr_span;

/// Represents a conversion operation.
#[doc(hidden)]
pub enum Conversion {
    Normal(Unit),
    Raw(Unit),
}

/// Parses the conversion operator, e.g. `~km2`.
pub fn value_conversion_operator<'a>(container: &'a Container) -> impl UParser<(Conversion, Span)> {
    move |input: Input<'a>| {
        let (remain_input, output) = alt((
            map(
                preceded(
                    pair(multispace0, tag("~!")),
                    optional_unit(container),
                ),
                Conversion::Raw
            ),
            map(
                preceded(
                    pair(multispace0, tag("~")),
                    optional_unit(container),
                ),
                Conversion::Normal
            ),
        ))(input)?;

        Ok((remain_input, (output, Span::from_inputs(input, remain_input))))
    }
}

/// Parses prefix unary operations.
pub fn incr_decr_prefix_operators(container: &Container) -> impl UParser<'_, ExprSpan> {
    to_expr_span(alt((
        map(
            preceded(spaced(tag("++")), assignable(container, AssignMode::Set)),
            Expr::PrefixIncr,
        ),
        map(
            preceded(spaced(tag("--")), assignable(container, AssignMode::Set)),
            Expr::PrefixDecr,
        ),
    )))
}
