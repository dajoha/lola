//! Contains all the parsers needed to parse lola code.

use super::is_keyword;

/// Wraps a parser into a closure in order to simplify rust nested types, and even more important,
/// break type recursion:
macro_rules! closure {
    ( $parser:expr ) => ( move |input: crate::parse::Input<'a>| $parser(input) )
}

mod nom {
    pub use nom::Parser;
    pub use nom::branch::alt;
    pub use nom::bytes::complete::tag;
    pub use nom::number::complete::float;
    pub use nom::character::complete::{
        anychar,
        one_of,
        digit1,
        multispace0,
        multispace1,
        not_line_ending,
    };
    pub use nom::combinator::{
        map,
        map_res,
        opt,
        recognize,
        verify,
        consumed,
    };
    pub use nom::multi::{
        many0,
        many1,
        separated_list0,
        separated_list1,
    };
    pub use nom::sequence::{
        pair,
        delimited,
        preceded,
        terminated,
        tuple,
        separated_pair,
    };
}

mod to_span;
mod blocks;
mod expressions;
mod flow_control;
mod functions;
mod generics;
mod literals;
mod operators;
mod units;
mod var_type;
mod arrays;
mod assignable;
mod dicts;

use super::types::*;
use super::input::*;
pub use to_span::*;
pub use blocks::*;
pub use expressions::*;
pub use flow_control::*;
pub use functions::*;
pub use generics::*;
pub use literals::*;
pub use operators::*;
pub use self::units::*;
pub use var_type::*;
pub use arrays::*;
pub use assignable::*;
pub use dicts::*;
