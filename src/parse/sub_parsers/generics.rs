///! Sub-parsers which are not really specific to the project.

use rug::Float;

use super::nom::{
    tag,
    anychar,
    multispace0,
    one_of,
    map,
    map_res,
    opt,
    alt,
    recognize,
    verify,
    pair,
    tuple,
    delimited,
    many0,
    many1,
    preceded,
};

use super::{UParser, UResult, Input};

/// Special characters that are allowed to be used inside identifiers, alongside alphabetic
/// characters.
const IDENTIFIER_SPECIAL_CHARS: &str = "_àâäéèêëîïôöùûüŷÿçαβδγοΑΒΔΓΟΩ";

/// Checks if the given character is valid inside an identifier (including the first char).
fn is_valid_identifier_char(c: &char) -> bool {
    c.is_alphabetic() || IDENTIFIER_SPECIAL_CHARS.contains(*c)
}

/// Removes underscore from a lola number.
fn filter_lola_digits(input: Input<'_>) -> String {
    input.text.chars().filter(|c| c != &'_').collect::<String>()
}

/// Parses one digits.
fn lola_first_digit(input: Input) -> UResult<'_, char> {
    one_of("0123456789")(input)
}

/// Parses one digit or an underscore.
fn lola_digit(input: Input) -> UResult<'_, char> {
    one_of("_0123456789")(input)
}

/// Parses an integer number with underscores allowed after the first digit.
fn lola_number(input: Input) -> UResult<'_, Input> {
    recognize(pair(
        lola_first_digit,
        many0(lola_digit)
    ))
    (input)
}

/// Parses an integer
pub fn integer<'a>(input: Input<'a>) -> UResult<'a, i64> {
    map_res(
        recognize(
            pair(
                opt(tag("-")),
                lola_number,
            )
        ),
        |s: Input<'a>| filter_lola_digits(s).parse()
    )
    (input)
}

/// Parses an signed integer and convert it to a Float.
pub fn float_from_integer(input: Input<'_>) -> UResult<Float> {
    map(
        recognize(
            pair(
                opt(tag("-")),
                lola_number,
            )
        ),
        |num: Input<'_>| float!(Float::parse(&filter_lola_digits(num)).unwrap())
    )(input)
}

/// Parses a float number which can be distinguished from an integer (at least a decimal point or
/// an exponent).
pub fn float_number_distinct_from_int(input: Input<'_>) -> UResult<Float> {
    map(
        recognize(tuple((
            opt(tag("-")),
            lola_number,
            alt((
                recognize(pair(
                    pair(
                        tag("."),
                        lola_number,
                    ),
                    opt(pair(
                        one_of("Ee"),
                        float_from_integer,
                    ))
                )),
                recognize(pair(
                    opt(pair(
                        tag("."),
                        lola_number,
                    )),
                    pair(
                        one_of("Ee"),
                        float_from_integer,
                    )
                ))
            ))
        ))),
        |v: Input<'_>| float!(Float::parse(&filter_lola_digits(v)).unwrap())
    )(input)
}

/// Parses a float number, with integer representation allowed.
pub fn float_number_including_int(input: Input<'_>) -> UResult<Float> {
    map(
        recognize(tuple((
            opt(tag("-")),
            lola_number,
            opt(pair(
                tag("."),
                lola_number,
            )),
            opt(pair(
                one_of("Ee"),
                float_from_integer,
            ))
        ))),
        |v: Input<'_>| float!(Float::parse(&filter_lola_digits(v)).unwrap())
    )(input)
}

/// Parses a classical identifier `[A-Za-z][A-Za-z0-9]*`.
pub fn identifier<'a>(input: Input<'a>) -> UResult<Input<'a>> {
    recognize(pair(
        many1(verify(anychar, is_valid_identifier_char)),
        many0(
            verify(anychar, |c| c.is_numeric() || is_valid_identifier_char(c))
        ),
    ))
    (input)
}

/// Parses the given word.
pub fn word<'a>(w: &'a str) -> impl UParser<'a, ()> {
    move |input: Input<'a>| {
        map(
            verify::<_, _, Input<'a>, _, _, _>(
                identifier,
                |id| id.text == w
            ),
            |_| ()
        )(input)
    }
}

/// Combinator for an element separated by spaces (zero or more).
pub fn spaced<'a, P, V>(parser: P) -> impl UParser<'a, V>
where
    P: UParser<'a, V>,
{
    delimited(
        multispace0,
        parser,
        multispace0,
    )
}

/// Allows an optional comma before the given parser. Used with closing tag parsers.
pub fn opt_comma<'a, P, V>(parser: P) -> impl UParser<'a, V>
where
    P: UParser<'a, V>,
{
    preceded(opt(spaced(tag(","))), spaced(parser))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_negative_number() {
        let r = float_number_distinct_from_int(Input::new("-4.2", 0));
        assert_eq!(r.unwrap().1, float_parsed!(-4.2));
        let r = float_number_including_int(Input::new("-4.2", 0));
        assert_eq!(r.unwrap().1, float_parsed!(-4.2));

        let r = float_number_distinct_from_int(Input::new("-4", 0));
        assert!(r.is_err());
        let r = float_number_including_int(Input::new("-4", 0));
        assert_eq!(r.unwrap().1, float_parsed!(-4));
    }

    #[test]
    fn parse_numbers_with_underscores() {
        let r = integer(Input::new("200_000", 0));
        assert_eq!(r.unwrap().1, 200000);
        let r = integer(Input::new("2__00_00_0___", 0));
        assert_eq!(r.unwrap().1, 200000);
        let r = integer(Input::new("__2", 0));
        assert!(r.is_err());

        let r = float_number_distinct_from_int(Input::new("-4__.2_1", 0));
        assert_eq!(r.unwrap().1, float_parsed!(-4.21));
        let r = float_number_including_int(Input::new("-4__.2_1", 0));
        assert_eq!(r.unwrap().1, float_parsed!(-4.21));
    }

    #[test]
    fn parse_exponent_number() {
        let r = float_number_including_int(Input::new("4.2e4", 0));
        assert_eq!(r.unwrap().1, float_parsed!(42000));
        let r = float_number_distinct_from_int(Input::new("4.2e4", 0));
        assert_eq!(r.unwrap().1, float_parsed!(42000));

        let r = float_number_including_int(Input::new("4e-2", 0));
        assert_eq!(r.unwrap().1, float_parsed!(0.04));
        let r = float_number_distinct_from_int(Input::new("4e-2", 0));
        assert_eq!(r.unwrap().1, float_parsed!(0.04));
    }

    #[test]
    fn parse_identifier() {
        let input = Input::new("foo", 0);
        let r = identifier(input).unwrap().1;
        assert_eq!(r.text, "foo");

        let input = Input::new("123foo", 0);
        let r = identifier(input);
        assert!(r.is_err());
    }

    #[test]
    fn parse_identifier_with_special_chars() {
        let input = Input::new("fête...", 0);
        let r = identifier(input).unwrap().1;
        assert_eq!(r.text, "fête");

        let input = Input::new("β_18()", 0);
        let r = identifier(input).unwrap().1;
        assert_eq!(r.text, "β_18");
    }
}
