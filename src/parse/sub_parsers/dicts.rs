use super::nom::{
    tag,
    separated_pair,
    separated_list0,
    consumed,
    delimited,
    map,
    alt,
    pair,
};

use crate::context::Container;
use crate::eval::{Expr, ExprSpan};
use crate::parse::types::UResult;
use crate::parse::{Input, Span};

use super::{UParser, opt_comma};
use super::{spaced, expression, identifier};

/// Parses a dict value.
pub fn dict(container: &Container) -> impl UParser<ExprSpan> {
    map(
        consumed(delimited(
            spaced(tag("{")),
            separated_list0(
                spaced(tag(",")),
                key_value(container),
            ),
            opt_comma(tag("}")),
        )),
        |(consumed, key_value_pairs)| {
            let span = Span::from(consumed);
            ExprSpan::with_span(Expr::Dict(key_value_pairs), span)
        }
    )
}

/// Parses the dot operator.
pub fn dot_oper(input: Input<'_>) -> UResult<(Span, ExprSpan)> {
    map(
        pair(
            spaced(tag(".")),
            spaced(identifier),
        ),
        |(dot, ident)| {
            let expr = Expr::Val(string!(ident.text));
            let expr_span = ExprSpan::with_span(expr, Span::from(ident));
            (Span::from(dot), expr_span)
        },
    )
    (input)
}

/// Parses a key/value pair of a dict.
fn key_value(container: &Container) -> impl UParser<(ExprSpan, ExprSpan)> {
    alt((
        separated_pair(
            key(container),
            spaced(tag(":")),
            expression(container),
        ),
        map(
            identifier,
            |ident| {
                let key = string_ident(ident);
                let value = ExprSpan::with_span(
                    Expr::Get(ident.text.to_owned()),
                    Span::from(ident)
                );
                (key, value)
            }
        ),
    ))
}

/// Parses a dict key.
fn key(container: &Container) -> impl UParser<ExprSpan> {
    alt((
        map(identifier, string_ident),
        delimited(spaced(tag("[")), expression(container), spaced(tag("]"))),
        expression(container),
    ))
}

fn string_ident(ident: Input<'_>) -> ExprSpan {
    ExprSpan::with_span(Expr::Val(string!(ident.text)), Span::from(ident))
}
