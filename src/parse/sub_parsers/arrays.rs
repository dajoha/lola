//! Parsers for array values.

use super::nom::{
    tag,
    delimited,
    tuple,
    separated_list0,
};

use crate::context::Container;
use crate::eval::{ExprSpan, Expr};
use crate::parse::{Span, Input};

use super::{UParser, opt_comma};
use super::{spaced, expression};
use super::to_expr_span_map;

/// Parses an array.
pub fn array(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        delimited(
            spaced(tag("[")),
            separated_list0(
                spaced(tag(",")),
                expression(container),
            ),
            opt_comma(tag("]")),
        ),
        Expr::Array
    )
}

/// Parses an index operator.
pub fn index<'a>(container: &'a Container) -> impl UParser<(ExprSpan, Span)> {
    move |input: Input<'a>| {
        let (remain_input, output) = tuple((
            spaced(tag("[")),
            closure!(expression(container)),
            spaced(tag("]")),
        ))(input)?;

        Ok((remain_input, (output.1, Span::from_inputs(input, remain_input))))
    }
}
