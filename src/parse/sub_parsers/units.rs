//! Parsers for units and quantities.

use std::collections::HashMap;
use std::iter;

use rug::Float;

use super::nom::{
    Parser,
    tag,
    anychar,
    one_of,
    many0,
    many1,
    pair,
    delimited,
    map,
    opt,
    verify,
    recognize,
    alt,
};

use crate::context::Container;
use crate::value::{Unit, CompoundQuantity};
use crate::parse::{Span, is_keyword};

use super::{UParser, UResult, UError, Input};
use super::{
    float_number_including_int,
    float_from_integer,
};

/// Special characters that are allowed to be used inside unit names, alongside alphabetic
/// characters.
const UNIT_SPECIAL_CHARS: &str = "μ°Ω$€";

/// Special characters that are allowed to be used inside quantity names, alongside alphabetic
/// characters.
const QUANTITY_SPECIAL_CHARS: &str = "_";

type ParsedExpUnit<'a> = (&'a str, Float);
type ParsedExpQuantity<'a> = (String, Float);

/// Checks if the given char is valid for a unit name
#[inline(always)]
fn is_valid_unit_char(c: &char) -> bool {
    c.is_alphabetic() || UNIT_SPECIAL_CHARS.contains(*c)
}

/// Checks if the given char is valid for a quantity name
#[inline(always)]
fn is_valid_quantity_char(c: &char) -> bool {
    c.is_alphabetic() || QUANTITY_SPECIAL_CHARS.contains(*c)
}

/// Parses a unit exponent.
fn exponent(input: Input<'_>) -> UResult<Float> {
    map(
        alt((
            float_from_integer,
            delimited(
                tag("'"),
                float_number_including_int,
                tag("'"),
            ),
        )),
        |n| float!(n)
    )(input)
}

/// Parses a basic unit, without its exponent.
fn basic_unit<'a>(input: Input<'a>) -> UResult<'a, Input<'a>> {
    verify(
        recognize(many1(verify(anychar, is_valid_unit_char))),
        |s: &Input<'a>| !is_keyword(s.text)
    )(input)
}

/// Parses a unit with optional exponential, e.g. `km2`.
fn exp_unit<'a>(input: Input<'a>) -> UResult<'a, ParsedExpUnit> {
    map(
        pair(basic_unit, opt(exponent)),
        |(unit, exp)| (unit.text, exp.unwrap_or_else(|| float_parsed!(1)))
    )(input)
}

/// Parses a unit with optional exponential, prepended by a multiply sign or divide
/// sign, like in a compound unit, e.g. `/s`, `.km-1`, ...
fn extra_exp_unit<'a>(input: Input<'a>) -> UResult<'a, ParsedExpUnit> {
    map(
        pair(one_of("./"), exp_unit),
        |(c, (unit, mut exp))| {
            if c == '/' {
                exp = -exp;
            };
            (unit, exp)
        }
    )(input)
}

/// Parses a whole unit, e.g., `m.s-1`.
pub fn optional_unit<'a>(container: &'a Container) -> impl UParser<'a, Unit> {
    move |input: Input<'a>| {
        let (i, ebus) =
            pair(
                opt(exp_unit),
                many0(extra_exp_unit)
            )
            .map(|(first_unit, other_units)|
                first_unit.into_iter()
                    .chain(other_units.into_iter())
                    .collect::<Vec<_>>()
            )
            .parse(input)?;
        match container.create_unit(&ebus) {
            Ok(unit) => Ok((i, unit)),
            Err(bu) => {
                let span = Span::from_inputs(input, i);
                Err(nom::Err::Failure(UError::UndefinedBasicUnit(bu.to_string(), span)))
            },
        }
    }
}

/// Parses a whole unit, e.g., `m.s-1`, with at least one exp unit.
pub fn mandatory_unit<'a>(container: &'a Container) -> impl UParser<'a, Unit> {
    move |input: Input<'a>| {
        let (i, ebus) =
            pair(
                exp_unit,
                many0(extra_exp_unit)
            )
            .map(|(first_unit, other_units)|
                iter::once(first_unit)
                    .chain(other_units.into_iter())
                    .collect::<Vec<_>>()
            )
            .parse(input)?;
        match container.create_unit(&ebus) {
            Ok(unit) => Ok((i, unit)),
            Err(bu) => {
                let span = Span::from_inputs(input, i);
                Err(nom::Err::Failure(UError::UndefinedBasicUnit(bu.to_string(), span)))
            },
        }
    }
}

/// Parses a quantity with optional exponential, e.g. `length2`.
fn exp_quantity<'a>(input: Input<'a>) -> UResult<'a, ParsedExpQuantity> {
    map(
        pair(
            recognize(many1(
                verify(anychar, is_valid_quantity_char)
            )),
            opt(exponent),
        ),
        |(q, exp)| (q.to_string(), exp.unwrap_or_else(|| float_parsed!(1)))
    )(input)
}

/// Parses a quantity with optional exponential, prepended by a multiply sign or divide
/// sign, like in a compound quantity, e.g. `/length`, `.length-1`, ...
fn extra_exp_quantity<'a>(input: Input<'a>) -> UResult<'a, ParsedExpQuantity> {
    map(
        pair(one_of("./"), exp_quantity),
        |(c, (q, mut exp))| {
            if c == '/' {
                exp = -exp;
            };
            (q, exp)
        }
    )(input)
}

/// Parses a whole compound quantity, e.g., `length/time`.
pub fn compound_quantity<'a>() -> impl UParser<'a, CompoundQuantity> {
    map(
        pair(
            opt(exp_quantity),
            many0(extra_exp_quantity)
        ),
        |(first_quantity, other_quantities)| {
            let hm = first_quantity.into_iter()
                .chain(other_quantities.into_iter())
                .collect::<HashMap<_, _>>();
            CompoundQuantity::from(hm)
        }
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_unit() {
        with_test_container!(container, {
            macro_rules! parse_unit {
                ($s:expr) => ( mandatory_unit(&container)(Input::new($s, 0)).unwrap().1 );
            }
            let u = parse_unit!("m2/s");
            assert_eq!(u, unit!(&container, m:2, s:-1));

            let u = parse_unit!("m.s-1");
            assert_eq!(u, unit!(&container, m:1, s:-1));

            let u = parse_unit!("m/s");
            assert_eq!(u, unit!(&container, m:1, s:-1));

            let u = parse_unit!("°");
            assert_eq!(u, container.create_unit(&[ ("°", float_parsed!(1)) ]).unwrap());
        })
    }

    #[test]
    fn test_parse_empty_unit() {
        with_test_container!(container, {
            macro_rules! parse_unit {
                ($s:expr) => ( optional_unit(&container)(Input::new($s, 0)).unwrap().1 );
            }
            let u = parse_unit!("!!");
            assert_eq!(u, unit!(&container, ));
        })
    }

    #[test]
    fn test_parse_bracket_exponent() {
        with_test_container!(container, {
            macro_rules! parse_unit {
                ($s:expr) => ( mandatory_unit(&container)(Input::new($s, 0)).unwrap().1 );
            }
            let u = parse_unit!("km'2'");
            assert_eq!(u, unit!(&container, km2));
            let u = parse_unit!("km'2.3'");
            assert_eq!(u, float_unit!(&container, km:float_parsed!(2.3)));
        })
    }

    #[test]
    fn test_parse_bad_unit() {
        with_test_container!(container, {
            macro_rules! parse_unit_res {
                ($s:expr) => ( mandatory_unit(&container)(Input::new($s, 0)) );
            }
            let r = parse_unit_res!("m2.bovis-1");
            assert!(matches!(r, Err(nom::Err::Failure(UError::UndefinedBasicUnit(_, _)))));
            let r = parse_unit_res!("if");
            assert!(matches!(r, Err(nom::Err::Error(UError::NomError(_)))));
            let r = parse_unit_res!("for");
            assert!(matches!(r, Err(nom::Err::Error(UError::NomError(_)))));
        })
    }
}
