//! Implements the [`Span`] struct.

use std::fmt;

use crate::context::Container;
use crate::parse::Input;

/// A structure storing the source code position (and length) of something (typically an
/// expression or an error).
///
/// The slice to the original string is not stored, in order to greatly simplify lifetime issues.
#[derive(Default, Clone, PartialEq)]
pub struct Span {
    pub line: Option<usize>,
    pub column: Option<usize>,
    pub offset: Option<usize>,
    pub length: Option<usize>,
    /// Index of the parsed input stored inside the container:
    pub parsed_input_index: Option<usize>,
}

impl Span {
    /// Creates a default span, with no information.
    pub fn new() -> Self {
        Self::default()
    }

    /// Creates a span starting from the beginning of the first span, and ending at the end of the
    /// second span.
    ///
    /// # Panics
    ///
    /// Panics if the two spans are not well ordered (first span before second span).
    pub fn from_interval(from: &Span, to: &Span) -> Self {
        Span {
            length: {
                if let (Some(from_offset), Some(to_offset), Some(to_len)) = (from.offset, to.offset, to.length) {
                    Some(to_offset - from_offset + to_len)
                } else {
                    None
                }
            },
            ..*from
        }
    }

    /// Creates a span starting from the beginning of the first span, and ending at the start of the
    /// second span.
    ///
    /// # Panics
    ///
    /// Panics if the two inputs are not well ordered (first input before second input).
    pub fn from_inputs(initial: Input<'_>, remaining: Input<'_>) -> Self {
        let (initial_ptr, remaining_ptr) = (initial.text.as_ptr(), remaining.text.as_ptr());

        let length = if initial_ptr <= remaining_ptr {
            Some(remaining_ptr as usize - initial_ptr as usize)
        } else {
            None
        };

        Span {
            line: Some(initial.line),
            column: Some(initial.column),
            offset: Some(initial.offset),
            length,
            parsed_input_index: Some(initial.parsed_input_index),
        }
    }

    pub fn display_with_source(&self, container: &Container) -> String {
        if let Some(index) = self.parsed_input_index {
            format!("{}, {}", container.get_parsed_input_source(index), self)
        } else {
            format!("{}", self)
        }
    }
}

impl From<Input<'_>> for Span {
    fn from(input: Input<'_>) -> Span {
        Span {
            line: Some(input.line),
            column: Some(input.column),
            offset: Some(input.offset),
            length: Some(input.len()),
            parsed_input_index: Some(input.parsed_input_index),
        }
    }
}

impl fmt::Debug for Span {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.line.is_none() && self.column.is_none() && self.offset.is_none() && self.length.is_none() {
            return write!(f, "(no span)")
        }

        let not_available = || "?".to_string();
        let check_available = |v: Option<usize>| {
            v.map(|v| v.to_string()).unwrap_or_else(not_available)
        };

        let line = self.line
            .map(|line| format!("#{}", line))
            .unwrap_or_else(not_available);

        write!(f, "{line}|{col} (len: {len}, ofs: {ofs})",
            line = line,
            col  = check_available(self.column),
            len  = check_available(self.length),
            ofs  = check_available(self.offset)
        )
    }
}

impl fmt::Display for Span {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.line.is_none() && self.column.is_none() && self.offset.is_none() && self.length.is_none() {
            return write!(f, "(not located)");
        }

        let mut infos = vec![];

        if let Some(line) = self.line {
            infos.push(format!("line {}", line));
        }
        if let Some(column) = self.column {
            infos.push(format!("column {}", column));
        }

        write!(f, "{}", infos.join(", "))
    }
}
