use std::fmt;
use std::path::PathBuf;

use getset::Getters;

/// The type of input source.
#[derive(Debug, Clone)]
pub enum ParsedInputSource {
    /// Module imported with the RT function `import()`. The 1st argument is the module name, the
    /// 2nd argument is the full path to the module (used by the default importer).
    Module(String, Option<PathBuf>),
    /// Code entered inline (in the interactive CLI for example).
    Inline(usize),
}

impl fmt::Display for ParsedInputSource {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Module(module, full_path) => {
                if let Some(full_path) = full_path {
                    write!(f, "Module '{}' ({})", module, full_path.display())
                } else {
                    write!(f, "Module '{}'", module)
                }
            }
            Self::Inline(index) => write!(f, "Inline #{}", index),
        }
    }
}

/// Used to store all parsed inputs inside a container. The main goal is to be able to give more
/// precise error messages, with the modaule name for example.
#[derive(Debug, Getters)]
pub struct ParsedInput {
    #[getset(get = "pub")]
    input: String,
    #[getset(get = "pub")]
    source: ParsedInputSource,
}

impl ParsedInput {
    pub fn new(input: String, source: ParsedInputSource) -> Self {
        Self { input, source }
    }
}
