//! Defines the [`Scope`] struct.

use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;

use crate::value::{Value, FuncOperand, IntoValue};
use crate::eval::Function;
use crate::value::Operand;

/// Defines a variable container which can have nested scope levels.
/// Local variables hide variables of the same name in parent scope levels.
#[derive(Debug, Clone)]
pub struct Scope {
    scope_levels: Vec<Rc<RefCell<ScopeLevel>>>,
}

impl Scope {
    /// Creates a new [`Scope`] instance, with one scope level (global scope) already added.
    pub fn new() -> Self {
        Self { scope_levels: vec![ Self::create_scope_level() ] }
    }

    /// Creates one scope level.
    #[inline]
    fn create_scope_level() -> Rc<RefCell<ScopeLevel>> {
        Rc::new(RefCell::new(ScopeLevel::new()))
    }

    /// Returns true if the current scope is global (only one scope level).
    #[inline]
    fn is_global(&self) -> bool {
        self.scope_levels.len() == 1
    }

    /// Creates a reverse iterator over the different scope levels.
    pub fn iter_rev(&self) -> impl Iterator<Item = &Rc<RefCell<ScopeLevel>>> {
        self.scope_levels.iter().rev()
    }

    /// Pushes a new empty scope level.
    pub fn push(&mut self) {
        self.scope_levels.push(Self::create_scope_level());
    }

    /// Pops the last scope level.
    pub fn pop(&mut self) {
        if self.scope_levels.len() > 1 {
            self.scope_levels.pop();
        }
    }

    /// Retrieves the given variable's value.
    pub fn var(&self, name: &str) -> Option<Rc<RefCell<Value>>> {
        self.iter_rev()
            .find_map(|scope| scope.borrow().var(name))
    }

    /// Checks if the given variable exists.
    pub fn has_var(&self, name: &str) -> bool {
        self.iter_rev()
            .any(|scope| scope.borrow().has_var(name))
    }

    /// Calls the given closure by giving to it a mutable reference to the value of a variable. If
    /// the variable does not exist (in the current scope or the parent scopes), returns an error
    /// [`ScopeError::VarDoesNotExist`], with one exception: if the current scope is the global
    /// scope, then undefined variables will be created on the fly.
    pub fn get_var_mut<T>(&mut self, name: &str, f: impl FnOnce(&mut Value) -> T) -> Result<T, ScopeError> {
        match self.var(name) {
            Some(v) => Ok(f(&mut v.borrow_mut())),
            None => {
                if self.is_global() {
                    // We unwrap because the variable can't exist here:
                    self.define_var(name.to_string(), void!()).unwrap();
                    let v = self.var(name).unwrap();
                    let mut v = v.borrow_mut();
                    Ok(f(&mut v))
                } else {
                    Err(ScopeError::VarDoesNotExist)
                }
            },
        }
    }

    /// Defines a new variable in the current scope.
    ///
    /// If a variable of the same name already exists in one of the current scope levels, then
    /// aborts and returns [`ScopeError::VarAlreadyExists`].
    pub fn define_var(&mut self, name: String, new_val: Value) -> Result<(), ScopeError> {
        if let Some(last_scope_level_ref) = self.scope_levels.last_mut() {
            let mut last_scope_level = last_scope_level_ref.borrow_mut();
            if last_scope_level.has_var(&name) {
                return Err(ScopeError::VarAlreadyExists);
            }
            last_scope_level.set_var(name, new_val);
        }
        Ok(())
    }

    /// Adds a named function in the current scope.
    pub fn add_named_function(&mut self, func: Function) -> Result<(), ScopeError> {
        let name = func.name().clone();
        let func_value = FuncOperand::new(func).into_value();
        self.define_var(name, func_value)
    }

    /// Creates an iterator over the variable names which start by the given prefix (useful for
    /// cli completion).
    pub fn vars_starting_with<'a>(&'a self, prefix: &'a str) -> Vec<String> {
        let dot_parts = prefix.split('.').collect::<Vec<_>>();

        if dot_parts.len() == 1 {
            self.iter_rev()
                .map(move |scope_level| scope_level.borrow().vars_starting_with(prefix))
                .flatten()
                .collect::<Vec<_>>()
        } else {
            if let Some(var) = self.var(dot_parts[0]) {
                let mut value = var.borrow().clone();
                let sub_parts = dot_parts.iter()
                    .skip(1)
                    .take(dot_parts.len().saturating_sub(2));
                for sub_part in sub_parts {
                    if let Some(dict) = value.get_dict() {
                        value = match dict.index(&string!(*sub_part)) {
                            Ok(v) => v,
                            Err(_) => return vec![],
                        }
                    } else {
                        return vec![];
                    }
                }
                if let Some(dict) = value.get_dict() {
                    let last_dot_part = dot_parts.last().unwrap();
                    let keys = dict.keys_starting_with(last_dot_part);
                    let first_parts = &prefix[0..prefix.len() - last_dot_part.len()];
                    return keys.iter()
                        .map(|k| format!("{}{}", first_parts, k))
                        .collect::<Vec<_>>();
                }
            }
            vec![]
        }
    }
}

impl Default for Scope {
    fn default() -> Self {
        Self::new()
    }
}

/// Errors that can happen when creating or assigning variables.
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ScopeError {
    VarDoesNotExist,
    VarAlreadyExists,
}

/// A scope level inside which one can define some variables and functions. A scope level is aimed
/// to be used via the [`Scope`] struct.
///
/// [`Scope`]: crate::context::Scope
#[derive(Debug, Default)]
pub struct ScopeLevel {
    pub variables: HashMap<String, Rc<RefCell<Value>>>,
}

impl ScopeLevel {
    /// Creates a new [`ScopeLevel`] instance.
    pub fn new() -> Self {
        Self::default()
    }

    /// Retrieves the given variable's value.
    pub fn var(&self, name: &str) -> Option<Rc<RefCell<Value>>> {
        self.variables.get(name).cloned()
    }

    /// Checks if the given variable exists.
    pub fn has_var(&self, name: &str) -> bool {
        self.variables.contains_key(name)
    }

    /// Sets the value of the given variable.
    pub fn set_var(&mut self, name: String, new_val: Value) {
        self.variables.insert(name, Rc::new(RefCell::new(new_val)));
    }

    /// Creates an iterator over the variable names which start by the given prefix (useful for
    /// cli completion).
    pub fn vars_starting_with<'a>(&'a self, prefix: &'a str) -> Vec<String> {
        self.variables.keys()
            .cloned()
            .filter(move |key| key.starts_with(prefix))
            .collect::<Vec<_>>()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[ignore]
    fn test_iter_vars_starting_with() {
        let mut scope_level = ScopeLevel::new();
        scope_level.set_var(s!("foo1"), v_true!());
        scope_level.set_var(s!("foo2"), v_true!());
        scope_level.set_var(s!("bar1"), v_true!());
        let mut varnames = scope_level.vars_starting_with("foo");
        varnames.sort();
        assert_eq!(varnames, vec![s!("foo1"), s!("foo2")]);
    }
}
