use std::{fmt, ops::RangeBounds};

use crate::value::Value;

use super::Container;

impl Container {
    /// Used by the RT function `config()`: applies changes to the [`FormatConfig`] configuration
    /// bound to this container.
    ///
    /// [`FormatConfig`]: crate::value::FormatConfig
    pub fn configure(&mut self, key: &str, value: Value) -> Result<(), ConfigureError> {
        match key {
            "digits" | "significant-digits" => {
                self.format_config.significant_digits = none_if_false(&value, || {
                    Ok(get_range_int(&value, 1..32000, key)? as i32)
                })?;
            },
            "underscores" => {
                self.format_config.underscores = none_if_false(&value, || {
                    Ok(get_range_int(&value, 1..32000, key)? as usize)
                })?;
            },
            "ellipsis" => {
                self.format_config.ellipsis = get_string(&value, key)?;
            },
            "multi-line" => {
                self.format_config.multi_line = get_boolean(&value, key)?;
            },
            "max-width" => {
                self.format_config.max_width = get_range_int(&value, 0.., key)? as usize;
            },
            "indent" => {
                self.format_config.indent = get_range_int(&value, 0..20, key)? as usize;
            },
            _ => return Err(ConfigureError::UnknownConfigKey(key.to_string())),
        }

        Ok(())
    }
}

/// Checks if a value is false.
fn is_false(value: &Value) -> bool {
    if let Some(b) = value.get_boolean() {
        if b.is_false() {
            return true;
        }
    }
    false
}

/// If a value is false, returns `None`, otherwise returns the result of the given closure.
fn none_if_false<F, V>(value: &Value, some: F) -> Result<Option<V>, ConfigureError>
where
    F: Fn() -> Result<V, ConfigureError>,
{
    let v = if is_false(value) {
        None
    } else {
        Some(some()?)
    };
    Ok(v)
}

/// Retrieves an int value, only if it is contained in the given range.
fn get_range_int(value: &Value, range: impl RangeBounds<i64>, key: &str) -> Result<i64, ConfigureError> {
    if let Some(n) = value.get_int() {
        let n = n.val();
        if range.contains(&n) {
            return Ok(n);
        }
    }
    Err(ConfigureError::BadConfigValue(key.to_string()))
}

/// Get a boolean value.
fn get_boolean(value: &Value, key: &str) -> Result<bool, ConfigureError> {
    if let Some(b) = value.get_boolean() {
        Ok(b.is_true())
    } else {
        Err(ConfigureError::BadConfigValue(key.to_string()))
    }
}

/// Get a string value.
fn get_string(value: &Value, key: &str) -> Result<String, ConfigureError> {
    if let Some(s) = value.get_str() {
        Ok(s.val_ref().to_string())
    } else {
        Err(ConfigureError::BadConfigValue(key.to_string()))
    }
}

/// Errors which can occur when calling the function [`Container::configure()`].
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ConfigureError {
    UnknownConfigKey(String),
    BadConfigValue(String),
}

impl fmt::Display for ConfigureError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::UnknownConfigKey(key) => write!(f, "Unknown configuration key: '{}'", key),
            Self::BadConfigValue(key) => write!(f, "Bad configuration value for '{}'", key),
        }
    }
}
