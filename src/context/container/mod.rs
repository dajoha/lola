//! Defines the [`Container`] struct.

use std::fmt;
use std::rc::Rc;
use std::cell::RefCell;
use std::io;
use std::path::PathBuf;

use rug::Float;
use getset::Getters;

use crate::value::{BaseUnit, ExpBaseUnit, FormatConfig, Quantity, Unit};
use crate::util::Writer;
use crate::context::{
    Importer,
    DefaultImporter,
};
use crate::parse::{ParsedInput, ParsedInputSource};

mod configure;

/// A container which holds definitions of physical units and quantities. It also handles the
/// output buffering (writing on stdout can be redirect to a buffer thanks to the [`Writer`] trait).
#[derive(Debug, Getters)]
pub struct Container {
    #[getset(get = "pub")]
    quantities: Vec<Rc<RefCell<Quantity>>>,
    #[getset(get = "pub")]
    base_units: Vec<Rc<BaseUnit>>,
    #[getset(get = "pub")]
    format_config: FormatConfig,
    writer: Rc<RefCell<dyn Writer>>,
    importer: Rc<RefCell<dyn Importer>>,
    /// Stores all the inputs parsed in the container.
    parsed_inputs: Rc<RefCell<Vec<ParsedInput>>>,
    /// Used to create new anonymous parsed inputs.
    inline_counter: Rc<RefCell<usize>>,
}

impl Default for Container {
    fn default() -> Self {
        Container::new(Container::default_writer(), Container::default_importer())
    }
}

impl Container {
    /// Creates a new [`Container`] instance.
    pub fn new<W: 'static + Writer, I: 'static + Importer>(writer: W, importer: I) -> Self {
        Self {
            quantities: vec![],
            base_units: vec![],
            format_config: FormatConfig::default(),
            writer: Rc::new(RefCell::new(writer)),
            importer: Rc::new(RefCell::new(importer)),
            parsed_inputs: Rc::new(RefCell::new(vec![])),
            inline_counter: Rc::new(RefCell::new(0)),
        }
    }

    pub fn default_writer() -> impl Writer {
        io::stdout()
    }

    pub fn buffered_writer() -> impl Writer {
        Vec::<u8>::new()
    }

    pub fn default_importer() -> impl Importer {
        DefaultImporter::new()
    }

    /// Returns the writer used by the `print()` library function.
    pub fn writer(&self) -> &Rc<RefCell<dyn Writer>> {
        &self.writer
    }

    /// Returns the configured code importer (used by the `import()` RT function).
    pub fn importer(&self) -> &Rc<RefCell<dyn Importer>> {
        &self.importer
    }

    /// Returns a copy of the current buffer output.
    pub fn get_buffer(&self) -> Option<String> {
        let writer = self.writer.borrow();
        writer.get_buffer()
    }

    /// Empties the writer buffer (if not stdout).
    pub fn empty_buffer(&self) {
        let mut writer = self.writer.borrow_mut();
        writer.empty_buffer();
    }

    /// Adds a quantity, e.g. "length", "mass"...
    pub fn add_quantity(&mut self, name: &str)
        -> Result<&Rc<RefCell<Quantity>>, QuantityAlreadyExists>
    {
        if self.strong_quantity(name).is_some() {
            return Err(QuantityAlreadyExists);
        }
        let q = Quantity::new(name);
        let q = Rc::new(RefCell::new(q));
        self.quantities.push(q);
        Ok(self.quantities.last().unwrap())
    }

    /// Finds a quantity by its name.
    pub fn strong_quantity(&self, name: &str) -> Option<&Rc<RefCell<Quantity>>> {
        self.quantities.iter()
            .find(|q| q.borrow().name() == name)
    }

    /// Adds a base unit to an existing quantity.
    pub fn add_base_unit(
        &mut self,
        name: &str,
        weight: Float,
        quantity: &Rc<RefCell<Quantity>>,
        equivalence: Option<Unit>,
    ) -> &Rc<BaseUnit> {
        if self.strong_base_unit(name).is_some() {
            panic!("Base unit name '{}' already exists.", name);
        }
        let bu = BaseUnit::new(name, quantity, weight, equivalence);
        let bu = Rc::new(bu);
        quantity.borrow_mut().add_base_unit(&bu);
        self.base_units.push(bu);
        self.base_units.last().unwrap()
    }

    /// Finds a base unit by its name.
    pub fn strong_base_unit(&self, name: &str) -> Option<&Rc<BaseUnit>> {
        self.base_units.iter()
            .find(|bu| bu.name() == name)
    }

    /// Creates a new unit, by giving a list of pairs with the following fields:
    ///  - name of an existing base unit (e.g: `km`)
    ///  - exponent for this base unit
    pub fn create_unit<'a>(&self, parts: &[(&'a str, Float)]) -> Result<Unit, &'a str> {
        let ebus = parts.iter()
            .map(|(bu_name, bu_exp)| -> Result<ExpBaseUnit, &str> {
                let bu = self.strong_base_unit(bu_name).ok_or(*bu_name)?;
                Ok(ExpBaseUnit::new(bu, bu_exp.clone()))
            })
            .collect::<Result<Vec<_>, &str>>()?;
        Ok(Unit::from(ebus))
    }

    /// Adds a new parsed input and returns its index.
    pub fn push_parsed_input(&self, input: &str, source: Option<(String, Option<PathBuf>)>) -> usize {
        let source = if let Some(source) = source {
            ParsedInputSource::Module(source.0, source.1)
        } else {
            let mut inline_counter = self.inline_counter.borrow_mut();
            let count = *inline_counter;
            *inline_counter += 1;
            ParsedInputSource::Inline(count)
        };
        let parsed_input = ParsedInput::new(input.to_string(), source);
        let mut parsed_inputs = self.parsed_inputs.borrow_mut();
        let index = parsed_inputs.len();
        parsed_inputs.push(parsed_input);

        index
    }

    /// Returns the given parsed input.
    pub fn get_parsed_input_source(&self, index: usize) -> ParsedInputSource {
        let parsed_inputs = self.parsed_inputs.borrow();
        parsed_inputs[index].source().clone()
    }
}

/// Error returned by [`Container::add_quantity()`].
#[derive(Debug)]
pub struct QuantityAlreadyExists;

impl fmt::Display for QuantityAlreadyExists {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Quantity already exists")
    }
}

impl std::error::Error for QuantityAlreadyExists { }
