//! This module contains functions which can be used to check function arguments and return types.

use std::{fmt, rc::Rc};

use crate::value::{CompoundQuantity, HashableValue, Value};
use super::{
    Function,
    function::FunctionArg,
    VarType,
    cast_int_to_number,
    get_number,
};

/// Errors that can occur when checking one argument.
#[derive(Debug, PartialEq)]
pub enum ArgError {
    MismatchTypes,
    Custom(String),
}

impl fmt::Display for ArgError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::MismatchTypes => write!(f, "Mismatch types"),
            Self::Custom(s)     => write!(f, "{}", s),
        }
    }
}

/// Helper which returns a [`ArgListError`] custom error.
macro_rules! custom_error {
    ($($tt:tt)*) => ( Err(err!(Arg:Custom(format!($($tt)*)))) )
}

pub trait ArgChecker: Fn(&mut Value, ArgKind, &Function) -> Result<(), ArgError> {}
impl<F> ArgChecker for F
    where F: Fn(&mut Value, ArgKind, &Function) -> Result<(), ArgError> {}

/// The kind of an argument.
#[derive(Copy, Clone)]
pub enum ArgKind<'a> {
    Arg(&'a FunctionArg),
    ReturnVal,
}

/// Checks if an argument is a void.
pub fn is_void(value: &mut Value, _: ArgKind, _: &Function) -> Result<(), ArgError> {
    if value.is_void() {
        Ok(())
    } else {
        Err(err!(Arg:MismatchTypes))
    }
}

/// Checks if an argument is a string.
pub fn is_string(value: &mut Value, _: ArgKind, _: &Function) -> Result<(), ArgError> {
    if value.is_str() {
        Ok(())
    } else {
        Err(err!(Arg:MismatchTypes))
    }
}

/// Checks if an argument is an array.
pub fn is_array(value: &mut Value, _: ArgKind, _: &Function) -> Result<(), ArgError> {
    if value.is_array() {
        Ok(())
    } else {
        Err(err!(Arg:MismatchTypes))
    }
}

/// Checks if an argument is a dict.
pub fn is_dict(value: &mut Value, _: ArgKind, _: &Function) -> Result<(), ArgError> {
    if value.is_dict() {
        Ok(())
    } else {
        Err(err!(Arg:MismatchTypes))
    }
}

/// Checks if an argument can be used as a dict key.
pub fn is_hashable_value(value: &mut Value, _: ArgKind, _: &Function) -> Result<(), ArgError> {
    if HashableValue::can_convert_from(value) {
        Ok(())
    } else {
        Err(err!(Arg:MismatchTypes))
    }
}

/// Checks if an argument is an int.
pub fn is_int(value: &mut Value, _: ArgKind, _: &Function) -> Result<(), ArgError> {
    if value.is_int() {
        Ok(())
    } else {
        Err(err!(Arg:MismatchTypes))
    }
}

/// Checks if an argument is a number.
pub fn is_number(value: &mut Value, _: ArgKind, _: &Function) -> Result<(), ArgError> {
    if value.is_num() {
        Ok(())
    } else {
        Err(err!(Arg:MismatchTypes))
    }
}

/// Checks if an argument is a boolean.
pub fn is_boolean(value: &mut Value, _: ArgKind, _: &Function) -> Result<(), ArgError> {
    if value.is_boolean() {
        Ok(())
    } else {
        Err(err!(Arg:MismatchTypes))
    }
}

/// Checks if an argument matches the given variable type.
pub fn has_type(vt: VarType) -> impl ArgChecker {
    move |value: &mut Value, _: ArgKind, _: &Function| -> Result<(), ArgError> {
        // TODO: these two variables should be static:
        let num_type = VarType::Num(None);
        let num_type_void_quantity = VarType::Num(Some(no_compound_quantity!()));

        if (vt == num_type || vt == num_type_void_quantity) && value.is_int() {
            // Automatic cast when a number is needed but an int is given:
            cast_int_to_number(value);
            Ok(())
        } else if value.has_type(&vt) {
            Ok(())
        } else {
            Err(err!(Arg:MismatchTypes))
        }
    }
}

/// Checks if an argument is a number with the given compound quantity.
pub fn has_quantity(expected_cq: CompoundQuantity) -> impl ArgChecker {
    move |value: &mut Value, arg_kind: ArgKind, func: &Function| -> Result<(), ArgError> {
        let value = get_number(value)?;
        let value_cq = CompoundQuantity::from(value.unit());
        if value_cq == expected_cq {
            Ok(())
        } else {
            match arg_kind {
                ArgKind::Arg(arg) => custom_error!(
                    "Function {}(): the parameter '{}' expects quantity '{}', but '{}' was given",
                    func.name(),
                    arg.name(),
                    expected_cq,
                    value_cq
                ),
                ArgKind::ReturnVal => custom_error!(
                    "Function {}(): the return value must have the quantity '{}', but '{}' was found",
                    func.name(),
                    expected_cq,
                    value_cq
                ),
            }
        }
    }
}

#[macro_export]
macro_rules! arg_checker_any {
    ( $first_checker:expr $(, $($checkers:expr),+ $(,)? )?) => {{
        use $crate::eval::arg_checker::or;
        let checker = $first_checker;
        $( $(let checker = or(checker, $checkers);)* )?
        checker
    }}
}

/// Combines two arg checkers by a "or" relation.
pub fn or(a: impl ArgChecker, b: impl ArgChecker) -> impl ArgChecker {
    move |value: &mut Value, arg_kind: ArgKind, func: &Function| -> Result<(), ArgError> {
        a(value, arg_kind, func).or_else(|_| b(value, arg_kind, func))
    }
}

/// Combines two arg checkers by a "or" relation.
pub fn dyn_or(a: Rc<dyn ArgChecker>, b: Rc<dyn ArgChecker>) -> impl ArgChecker {
    move |value: &mut Value, arg_kind: ArgKind, func: &Function| -> Result<(), ArgError> {
        a(value, arg_kind, func).or_else(|_| b(value, arg_kind, func))
    }
}
