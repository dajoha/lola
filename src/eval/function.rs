//! Handles runtime functions.

use std::fmt;
use std::rc::Rc;
use std::cell::RefCell;
use std::convert::TryFrom;
use std::collections::HashSet;

use getset::Getters;
use itertools::izip;
use derive_builder::Builder;

use crate::eval::arg_checker::ArgKind;
use crate::value::Value;
use crate::context::{Scope, ContextRef};
use crate::eval::{
    ExprSpan,
    ExprError,
    ExprResult,
    ExprResultSpanUtil,
    ArgListChecker,
    arg_list_checker::ArgListError,
    ArgChecker,
    ArgError,
};
use crate::parse::Span;
use crate::err;
use crate::numbers::IntConversionError;



/// Represents a function which can be added to a [`Scope`].
#[derive(Getters, Clone, Builder)]
#[builder(public)]
pub struct Function {
    #[getset(get = "pub")]
    #[builder(setter(into))]
    name: String,

    #[getset(get = "pub")]
    #[builder(default)]
    args: FunctionArgs,

    #[builder(setter(custom))]
    body: Rc<RefCell<FunctionBody>>,

    #[builder(default)]
    #[builder(setter(custom))]
    arg_list_checker: Option<Rc<dyn ArgListChecker>>,

    #[builder(default)]
    #[builder(setter(custom))]
    return_checker: Option<Rc<dyn ArgChecker>>,
}

impl Function {
    /// Gets the number of arguments of the function.
    pub fn nb_args(&self) -> usize {
        self.args.0.len()
    }

    /// Calls the function.
    pub fn call(
        &self,
        cxt: &mut ContextRef,
        args: Vec<Value>,
        caller_span: &Span
    ) -> CallResult
    {
        match self.real_call(cxt, args, caller_span) {
            Ok(Ok(mut value)) => {
                if let Some(checker) = &self.return_checker {
                    match checker(&mut value, ArgKind::ReturnVal, self) {
                        Ok(()) => Ok(Ok(value)),
                        Err(e) => Err(err!(Call:ReturnVal(e))),
                    }
                } else {
                    Ok(Ok(value))
                }
            },
            other => other,
        }
    }

    /// Calls the function, without result check.
    fn real_call(
        &self,
        cxt: &mut ContextRef,
        mut args: Vec<Value>,
        caller_span: &Span
    ) -> CallResult
    {
        // Applies the arg checker of each argument:
        izip!(self.args.iter(), args.iter_mut())
            .map(|(func_arg, arg)| func_arg.check(arg, self) )
            .collect::<Result<_, _>>()?;

        // Applies the global function arg checker:
        if let Some(arg_list_checker) = &self.arg_list_checker {
            arg_list_checker(&mut args, self).map_err(|err| err!(Call:ArgList(err)))?;
        }

        // Calls the function:
        let to_eval = match self.body.try_borrow_mut() {
            Ok(mut ref_mut) => match &mut *ref_mut {
                FunctionBody::Native(func) => {
                    return Ok(func(&args, cxt));
                },
                FunctionBody::NativeToEval(func) => {
                    match func(&args, cxt, caller_span) {
                        Ok(to_eval) => to_eval,
                        Err(expr_err) => return Ok(Err(expr_err)),
                    }
                }
                FunctionBody::UserDefined(func_code, func_scope) => {
                    return self.call_user_defined(cxt, func_code, func_scope, args);
                },
            },
            Err(_) => {
                return Err(err!(Call:RecursionNotAllowed));
            },
        };

        // This special case is here only for the special `import()` RT function: in order to allow
        // nested imports, the code must be evaluated outside the `borrow_mut()` of self.body.
        Ok(to_eval.eval(cxt))
    }

    /// Calls a user-defined function.
    fn call_user_defined(
        &self,
        cxt: &mut ContextRef,
        func_code: &mut ExprSpan,
        func_scope: &mut Scope,
        args: Vec<Value>
    ) -> CallResult {
        let mut func_cxt = ContextRef::new(cxt.container, func_scope);
        func_cxt.scope.push();

        izip!(self.args.iter(), args.iter())
            .for_each(|(func_arg, arg)| {
                func_cxt.scope.define_var(func_arg.name.clone(), arg.clone()).unwrap();
            });

        let func_result = func_code.eval(&mut func_cxt);

        let expr_result = match func_result.get_unspan_err() {
            Some(&ExprError::Return(_)) => {
                Ok(unwrap_expr_error_value!(func_result, Return))
            },
            Some(&ExprError::Break(_)) => {
                let expr_error = match func_result.get_err_span() {
                    Some(span) => ExprError::Spanned(span, Box::new(ExprError::BreakFunc)),
                    None       => ExprError::BreakFunc,
                };
                Err(expr_error)
            },
            Some(&ExprError::Continue(_)) => {
                let expr_error = match func_result.get_err_span() {
                    Some(span) => ExprError::Spanned(span, Box::new(ExprError::ContinueFunc)),
                    None       => ExprError::ContinueFunc,
                };
                Err(expr_error)
            },
            _ => func_result,
        };

        func_cxt.scope.pop();

        Ok(expr_result)
    }
}

impl fmt::Debug for Function {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let args = self.args.0.iter()
            .map(|arg| arg.name.clone())
            .collect::<Vec<_>>()
            .join(", ");
        write!(f, "Function: {}({})", self.name, args)
    }
}



impl FunctionBuilder {
    pub fn body(&mut self, body: FunctionBody) -> &mut Self {
        self.body = Some(Rc::new(RefCell::new(body)));
        self
    }

    pub fn native_body(&mut self, body: Box<dyn NativeFunction>) -> &mut Self {
        self.body = Some(Rc::new(RefCell::new(FunctionBody::Native(body))));
        self
    }

    pub fn arg_list_checker(&mut self, value: Rc<dyn ArgListChecker>) -> &mut Self {
        self.arg_list_checker = Some(Some(value));
        self
    }

    pub fn return_checker(&mut self, value: Rc<dyn ArgChecker>) -> &mut Self {
        self.return_checker = Some(Some(value));
        self
    }
}



/// Errors which can occur when a function is created.
#[derive(Debug, PartialEq)]
pub enum FunctionError {
    ArgNamesAreNotUnique,
}

impl fmt::Display for FunctionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::ArgNamesAreNotUnique => write!(f, "Function definition: argument names must be unique"),
        }
    }
}



/// Definition of the arguments of a function.
#[derive(Debug, Clone, Default)]
pub struct FunctionArgs ( Vec<FunctionArg> );

impl FunctionArgs {
    /// Returns an iterator over the function arguments.
    pub fn iter(&self) -> impl Iterator<Item=&FunctionArg> {
        self.0.iter()
    }

    /// Checks if the given argument names are unique.
    fn check_unique_names(names: &[String]) -> Result<(), FunctionError> {
        let unique_names = names.iter().collect::<HashSet<_>>();
        if names.len() != unique_names.len() {
            Err(FunctionError::ArgNamesAreNotUnique)
        } else {
            Ok(())
        }
    }
}

impl TryFrom<Vec<String>> for FunctionArgs {
    type Error = FunctionError;

    fn try_from(names: Vec<String>) -> Result<FunctionArgs, Self::Error> {
        FunctionArgs::try_from(&names)
    }
}

impl TryFrom<&Vec<String>> for FunctionArgs {
    type Error = FunctionError;

    fn try_from(names: &Vec<String>) -> Result<FunctionArgs, Self::Error> {
        Self::check_unique_names(names)?;

        Ok(FunctionArgs (
            names.iter()
                .map(|name| FunctionArg::from(name.as_ref()))
                .collect::<Vec<_>>()
        ))
    }
}

impl TryFrom<Vec<FunctionArg>> for FunctionArgs {
    type Error = FunctionError;

    fn try_from(args: Vec<FunctionArg>) -> Result<FunctionArgs, Self::Error> {
        FunctionArgs::try_from(&args)
    }
}

impl TryFrom<&Vec<FunctionArg>> for FunctionArgs {
    type Error = FunctionError;

    fn try_from(args: &Vec<FunctionArg>) -> Result<FunctionArgs, Self::Error> {
        let names = args.iter().map(|arg| arg.name.clone()).collect::<Vec<_>>();
        Self::check_unique_names(&names)?;

        Ok(FunctionArgs (
            args.clone()
        ))
    }
}

impl TryFrom<Vec<(String, Option<Rc<dyn ArgChecker>>)>> for FunctionArgs {
    type Error = FunctionError;

    fn try_from(init_args: Vec<(String, Option<Rc<dyn ArgChecker>>)>) -> Result<FunctionArgs, Self::Error> {
        let names = init_args.iter()
            .map(|(n, _)| n.clone())
            .collect::<Vec<_>>();
        Self::check_unique_names(&names)?;

        Ok(FunctionArgs (
            init_args.into_iter()
                .map(|(name, checker)| FunctionArg::from((name.as_ref(), checker)))
                .collect::<Vec<_>>()
        ))
    }
}



/// Definition of one function argument.
#[derive(Getters, Clone)]
pub struct FunctionArg {
    /// Name of the argument in the function definition.
    #[getset(get = "pub")]
    name: String,

    /// Argument checker.
    checker: Option<Rc<dyn ArgChecker>>,
}

impl FunctionArg {
    /// Creates a new function argument definition.
    pub fn new(name: String, checker: Option<Rc<dyn ArgChecker>>) -> Self {
        Self {
            name,
            checker,
        }
    }

    /// Checks if the given argument value fits the needs of the argument checker.
    pub fn check(&self, arg: &mut Value, func: &Function) -> Result<(), CallError> {
        if let Some(checker) = &self.checker {
            checker(arg, ArgKind::Arg(self), func).map_err(|err| err!(Call:ArgList:Arg(err)))
        } else {
            Ok(())
        }
    }
}

impl From<&str> for FunctionArg {
    fn from(s: &str) -> FunctionArg {
        FunctionArg::new(s.to_owned(), None)
    }
}

impl From<(&str, Option<Rc<dyn ArgChecker>>)> for FunctionArg {
    fn from((s, c): (&str, Option<Rc<dyn ArgChecker>>)) -> FunctionArg {
        FunctionArg::new(s.to_owned(), c)
    }
}

impl fmt::Debug for FunctionArg {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}



/// The body of a function.
pub enum FunctionBody {
    Native(Box<dyn NativeFunction>),
    /// Only used by the special `import()` RT function.
    NativeToEval(Box<dyn NativeToEvalFunction>),
    UserDefined(ExprSpan, Scope),
}

impl fmt::Debug for FunctionBody {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Native(_)         => write!(f, "[native]"),
            Self::NativeToEval(_)   => write!(f, "[native-to-eval]"),
            Self::UserDefined(_, _) => write!(f, "[user]"),
        }
    }
}



/// The protoype to use to create a native function.
pub trait NativeFunction: FnMut(&[Value], &mut ContextRef) -> ExprResult {}

impl<F> NativeFunction for F
where F: FnMut(&[Value], &mut ContextRef) -> ExprResult
{}



/// The protoype to use to create a native function which will return an expression to eval, like
/// the special `import()` RT function.
pub trait NativeToEvalFunction: FnMut(&[Value], &mut ContextRef, &Span) -> Result<ExprSpan, ExprError> {}

impl<F> NativeToEvalFunction for F
where F: FnMut(&[Value], &mut ContextRef, &Span) -> Result<ExprSpan, ExprError>
{}



/// The result a function call.
pub type CallResult = Result<ExprResult, CallError>;



/// Errors which can happen when calling a function.
#[derive(Debug, PartialEq)]
pub enum CallError {
    /// The arguments checker detected an error.
    ArgList(ArgListError),
    /// The return value checker detected an error.
    ReturnVal(ArgError),
    /// A conversion to an integer didn't succeed.
    IntConversion(IntConversionError),
    /// A conversion to a number didn't succeed.
    NumberConversion(String),
    /// A conversion failed because the bounds of the type to convert was too small too hold the
    /// value.
    ValueOutOfBounds,
    /// Recursive call was detected.
    RecursionNotAllowed,
}

impl fmt::Display for CallError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::ArgList(err)        => write!(f, "Function arguments: {}", err),
            Self::ReturnVal(err)      => write!(f, "Return value: {}", err),
            Self::IntConversion(err)  => write!(f, "{}", err),
            Self::NumberConversion(s) => write!(f, "Unable to convert \"{}\" to a number", s),
            Self::ValueOutOfBounds    => write!(f, "Value is out of bounds"),
            Self::RecursionNotAllowed => write!(f, "Recursion call detected: sorry, this is not available for now!"),
        }
    }
}



#[cfg(test)]
mod tests {
    use super::*;
    use crate::util::test_common::*;
    use crate::{func, args_eval, body};

    #[test]
    fn function_arg_from() {
        let arg = FunctionArg::from("hi");
        assert_eq!(arg.name, "hi");

        let args = FunctionArgs::try_from(vec![s!("hello"), s!("world")]).unwrap();
        assert_eq!(args.0[0].name, "hello");
        assert_eq!(args.0[1].name, "world");
    }

    #[test]
    fn call_function() {
        with_test_context!(cxt, {
            let f = func!(
                f,
                args_eval!(FunctionArgs::try_from(vec![s!("a")]).unwrap()),
                body!(|values, cxt| {
                    Ok(values[0].add(&number!(cxt.container, 3 km)).unwrap())
                })
            );

            let args = vec![ number!(cxt.container, 2 km) ];
            let span = Span::default();
            let r = f.call(&mut cxt, args, &span).unwrap();
            assert_eq!(r, Ok(number!(cxt.container, 5 km)));
        })
    }

    #[test]
    fn user_func_return() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                let f = fn(n) {
                    if (n > 10) return false;
                    let res = 1;
                    while (n != 0) res *= n--;
                    res
                }
            "#);
            assert_eq!(eval!(cxt, "f(4)"), int!(24));
            assert_eq!(eval!(cxt, "f(11)"), v_false!());

            eval!(cxt, r#"
                f = fn(n) {
                    let res = 1;
                    while (n != 0) {
                        res *= n--;
                        if (res > 100) return false;
                    };
                    res
                }
            "#);
            assert_eq!(eval!(cxt, "f(4)"), int!(24));
            assert_eq!(eval!(cxt, "f(11)"), v_false!());
        })
    }

    #[test]
    fn user_func_scope() {
        with_test_context!(cxt, {
            let v = eval!(cxt, r#"
                let n = 7;
                let f = fn() { n };
                {
                    let n = 14; // This local var should not be accessible by the
                                // body of the function call just below.
                    f()
                }
            "#);
            assert_eq!(v, int!(7));
        })
    }

    #[test]
    fn user_func_return_outside_func() {
        with_test_context!(cxt, {
            let err = eval_wrapped!(cxt, "return").map_unspan_err();
            assert!(matches!(err, Err(ExprError::Return(_))));
        })
    }

    #[test]
    fn func_args_with_same_name() {
        let r = FunctionArgs::try_from(vec![s!("a"), s!("b"), s!("a")]);
        let err = r.unwrap_err();
        assert_eq!(err, FunctionError::ArgNamesAreNotUnique);
    }

    #[test]
    fn recursion_error() {
        with_test_context!(cxt, {
            let r = eval_err!(cxt, "let rec = fn() { rec() }; rec()");
            assert_eq!(r, err!(Expr:Call:RecursionNotAllowed));
        })
    }

    #[test]
    fn argument_type() {
        with_test_context!(cxt, {
            eval!(cxt, "let f = fn(a: number) a");
            assert_eq!(eval!(cxt, "f(3.0)"), number!(cxt.container, 3));
            assert_eq!(eval!(cxt, "f(3)"), number!(cxt.container, 3)); // cast in to num
            assert_eq!(
                eval_err!(cxt, "f(true)"),
                err!(Expr:Call:ArgList:Arg:MismatchTypes)
            );
            assert_eq!(
                eval_err!(cxt, "f(\"hi\")"),
                err!(Expr:Call:ArgList:Arg:MismatchTypes)
            );

            eval!(cxt, "f = fn(a: boolean) a");
            assert_eq!(eval!(cxt, "f(true)"), v_true!());
            assert_eq!(
                eval_err!(cxt, "f(3)"),
                err!(Expr:Call:ArgList:Arg:MismatchTypes)
            );
            assert_eq!(
                eval_err!(cxt, "f(\"hi\")"),
                err!(Expr:Call:ArgList:Arg:MismatchTypes)
            );
        })
    }

    #[test]
    fn function_return_type() {
        with_test_context!(cxt, {
            eval!(cxt, "fn test(v) { v }");
            assert_eq!(eval!(cxt, "test(4)"), int!(4));
            assert_eq!(eval!(cxt, "test(3.0)"), number!(cxt.container, 3));
        });
        with_test_context!(cxt, {
            eval!(cxt, "fn test(v): int { v }");
            assert_eq!(eval!(cxt, "test(4)"), int!(4));
            assert_eq!(eval_err!(cxt, "test(3.0)"), err!(Expr:Call:ReturnVal:MismatchTypes));
        })
    }

    #[test]
    fn function_multiple_return_type() {
        with_test_context!(cxt, {
            eval!(cxt, "fn test(v): int|string { v }");
            assert_eq!(eval!(cxt, "test(4)"), int!(4));
            assert_eq!(eval!(cxt, "test(\"foo\")"), string!("foo"));
            assert_eq!(eval_err!(cxt, "test(3.0)"), err!(Expr:Call:ReturnVal:MismatchTypes));
            assert_eq!(eval_err!(cxt, "test(())"), err!(Expr:Call:ReturnVal:MismatchTypes));
        });
        with_test_context!(cxt, {
            eval!(cxt, "fn test(v): ?string { v }");
            assert_eq!(eval!(cxt, "test(())"), void!());
            assert_eq!(eval!(cxt, "test(\"foo\")"), string!("foo"));
            assert_eq!(eval_err!(cxt, "test(3)"), err!(Expr:Call:ReturnVal:MismatchTypes));
        })
    }
}
