//! Defines the [`VarType`] struct.

use crate::value::CompoundQuantity;

/// The type of a variable. It can also actually represent a "type pattern", as used in a function
/// argument definition. That's why the [`VarType::Num`] holds its quantity as an option, because a
/// function argument may need either a number of any quantity, or a number with a determined
/// quantity.
#[derive(Debug, Clone, PartialEq)]
pub enum VarType {
    Void,
    Num(Option<CompoundQuantity>),
    Int,
    Boolean,
    Str,
    Func,
    Array,
    Dict,
}

impl VarType {
    /// Retuns a string describing the main type of this type, e.g.: `number`, `string`,
    /// `function`...
    pub fn main_type(&self) -> &str {
        match self {
            Self::Void    => "void",
            Self::Num(_)  => "number",
            Self::Int     => "int",
            Self::Boolean => "boolean",
            Self::Str     => "string",
            Self::Func    => "function",
            Self::Array   => "array",
            Self::Dict    => "dict",
        }
    }

    /// Returns a string describing the whole type, e.g.: `string`, `number(length)`...
    pub fn whole_type(&self, format_options: FormatOptions) -> String {
        let main_type = self.main_type();
        match self {
            Self::Num(Some(cq)) => if format_options.compact_numbers {
                format!("{{{}}}", cq)
            } else {
                format!("{}({})", main_type, cq)
            },
            _ => main_type.to_string()
        }
    }
}

/// Options for the method [`VarType::whole_type()`].
#[derive(Debug, Default)]
pub struct FormatOptions {
    /// If `true`, then the quantity of a number will be formatted using the compact syntax, e.g.:
    /// `{length}` instead of `number(length)`.
    pub compact_numbers: bool,
}
