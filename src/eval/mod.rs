//! This module holds everything related to the evaluation of expressions.

pub mod assignable;
#[macro_use] pub mod expr_error;
pub mod expr;
pub mod expr_span;
#[macro_use] pub mod function;
pub mod arg_checker;
pub mod arg_list_checker;
pub mod var_type;
pub mod func_macros;

use std::mem;

#[doc(inline)]
pub use assignable::*;
#[doc(inline)]
pub use expr::*;
#[doc(inline)]
pub use expr_span::*;
#[doc(inline)]
pub use expr_error::*;
#[doc(inline)]
pub use function::{Function, FunctionBuilder};
#[doc(inline)]
pub use arg_checker::ArgChecker;
#[doc(inline)]
pub use arg_list_checker::ArgListChecker;
#[doc(inline)]
pub use var_type::VarType;

use crate::value::{NumOperand, Value, IntoValue};
use arg_checker::ArgError;

/// Casts an integer value to a number value.
fn cast_int_to_number(int_value: &mut Value) {
    let mut num_value = NumOperand::from(int_value.unwrap_int().val()).into_value();
    mem::swap(int_value, &mut num_value);
}

/// Checks if a value is a number and returns it, otherwise returns an error.
/// Used for arg checkers.
fn get_number(value: &mut Value) -> Result<&NumOperand, ArgError> {
    match value {
        Value::Num(n) => Ok(n),
        Value::Int(_) => {
            cast_int_to_number(value);
            Ok(value.unwrap_num())
        },
        _ => Err(err!(Arg:MismatchTypes)),
    }
}
