//! Helper macros to create errors.

/// Creates nested errors.
#[macro_export]
#[doc(hidden)]
macro_rules! err {
    ( Expr:Operation:$($tt:tt)* )          => ( $crate::eval::ExprError::Operation(err!(Operation:$($tt)*)) );
    ( Expr:Function:$($tt:tt)* )           => ( $crate::eval::ExprError::Function(err!(Function:$($tt)*)) );
    ( Expr:Dict:$($tt:tt)* )               => ( $crate::eval::ExprError::Dict(err!(Dict:$($tt)*)) );
    ( Expr:Call:ArgList:$($tt:tt)* )       => ( $crate::eval::ExprError::Call(err!(Call:ArgList:$($tt)*)) );
    ( Expr:Call:ReturnVal:$($tt:tt)* )     => ( $crate::eval::ExprError::Call(err!(Call:ReturnVal:$($tt)*)) );
    ( Expr:Call:IntConversion:$($tt:tt)* ) => ( $crate::eval::ExprError::Call(err!(Call:IntConversion:$($tt)*)) );
    ( Expr:Call:$($tt:tt)* )               => ( $crate::eval::ExprError::Call(err!(Call:$($tt)*)) );
    ( Expr:$($tt:tt)* )                    => ( $crate::eval::ExprError::$($tt)* );
    ( Function:$($tt:tt)* )                => ( $crate::eval::function::FunctionError::$($tt)* );
    ( Operation:Dict:$($tt:tt)* )          => ( $crate::value::OperationError::Dict(err!(Dict:$($tt)*)) );
    ( Operation:$($tt:tt)* )               => ( $crate::value::OperationError::$($tt)* );
    ( Dict:$($tt:tt)* )                    => ( $crate::value::DictError::$($tt)* );
    ( Call:ArgList:$($tt:tt)* )            => ( $crate::eval::function::CallError::ArgList(err!(ArgList:$($tt)*)) );
    ( Call:ReturnVal:$($tt:tt)* )          => ( $crate::eval::function::CallError::ReturnVal(err!(Arg:$($tt)*)) );
    ( Call:IntConversion:$($tt:tt)* )      => ( $crate::eval::function::CallError::IntConversion(err!(IntConversion:$($tt)*)) );
    ( Call:$($tt:tt)* )                    => ( $crate::eval::function::CallError::$($tt)* );
    ( ArgList:Custom($msg:expr) )          => ( $crate::eval::arg_list_checker::ArgListError::Custom(String::from($msg)) );
    ( ArgList:Arg:$($tt:tt)* )             => ( $crate::eval::arg_list_checker::ArgListError::Arg(err!(Arg:$($tt)*)) );
    ( ArgList:$($tt:tt)* )                 => ( $crate::eval::arg_list_checker::ArgListError::$($tt)* );
    ( Arg:Custom($msg:expr) )              => ( $crate::eval::ArgError::Custom(String::from($msg)) );
    ( Arg:$($tt:tt)* )                     => ( $crate::eval::arg_checker::ArgError::$($tt)* );
    ( IntConversion:$($tt:tt)* )           => ( $crate::util::numbers::IntConversionError::$($tt)* );
}
