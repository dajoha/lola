//! Contains all the helper macros used in the library.

#[macro_use] mod value;
#[macro_use] mod eval;
#[macro_use] mod error;

/// Creates `String` instances.
#[cfg(test)]
macro_rules! s {
    ()        => { String::new() };
    ($e:expr) => { String::from($e) };
}
