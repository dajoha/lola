//! Shortcut macros for quickly parsing and evaluating a string expression.

/// Parses, evaluates and unwraps the given string expression.
#[macro_export]
macro_rules! eval {
    ( $cxt:expr, $expr:expr ) => {
        parse!($cxt.container, $expr).eval(&mut $cxt).unwrap()
    }
}

/// Parses and evaluates the given string expression, without unwrapping.
#[macro_export]
macro_rules! eval_wrapped {
    ( $cxt:ident, $expr:expr) => {
        parse!($cxt.container, $expr).eval(&mut $cxt)
    }
}
