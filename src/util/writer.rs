//! Defines the [`Writer`] trait.

use std::io;
use std::fmt::Debug;

/// A wrapper around [`std::io::Write`]: allows to buffer the output of the `print()` runtime
/// function; used inside [`Container`].
///
/// [`Container`]: crate::context::Container
pub trait Writer: io::Write + Debug {
    /// Returns a copy of the output buffer, if available.
    fn get_buffer(&self) -> Option<String> {
        None
    }

    /// Empties the buffer, if available.
    fn empty_buffer(&mut self) {
    }
}

impl Writer for io::Stdout {}

impl Writer for Vec<u8> {
    fn get_buffer(&self) -> Option<String> {
        Some(String::from_utf8(self.clone()).unwrap())
    }

    fn empty_buffer(&mut self) {
        self.clear()
    }
}
