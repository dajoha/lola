//! Implements the [`BaseUnit`] struct.

use std::cell::RefCell;
use std::rc::{Rc, Weak};

use rug::Float;

use crate::value::{Quantity, Unit};

/// Represents an atomic unit, e.g. meters, hours, seconds...
#[derive(Debug)]
pub struct BaseUnit {
    name: String,
    quantity: Weak<RefCell<Quantity>>,
    preprocessed_weight: Float,
    /// See [`Self::equivalence()`].
    equivalence: Option<Unit>,
}

impl BaseUnit {
    /// Creates a new [`BaseUnit`] instance.
    pub fn new(
        name: &str,
        quantity: &Rc<RefCell<Quantity>>,
        weight: Float,
        equivalence: Option<Unit>,
    ) -> Self
    {
        let preprocessed_weight = match &equivalence {
            Some(unit) => float!(&weight * &unit.weight()),
            None => weight,
        };
        Self {
            name: name.to_string(),
            quantity: Rc::downgrade(quantity),
            equivalence,
            preprocessed_weight,
        }
    }

    /// Helper to access to the upgraded version of the weak reference to the unit's quantity.
    pub fn strong_quantity(&self) -> Rc<RefCell<Quantity>> {
        self.quantity.upgrade().unwrap()
    }

    /// Helper to access to the the weak reference to the unit's quantity.
    pub fn weak_quantity(&self) -> &Weak<RefCell<Quantity>> {
        &self.quantity
    }

    /// Getter for `name`
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Getter for `weight`
    pub fn weight(&self) -> &Float {
        &self.preprocessed_weight
    }

    /// Get the equivalence unit. For base units like `m`, `km`, `s`, it is `None` because those
    /// units are fundamental base units. But for a derived unit like newtons (`N`), it will return
    /// the equivalent unit expressed in base units: `kg·m/s2`.
    ///
    /// This is used to determine if two units are equivalent, e.g. `N·m` will be detected as
    /// equivalent to `J` (joules).
    pub fn equivalence(&self) -> Option<&Unit> {
        self.equivalence.as_ref()
    }

    // Warning: this code is working but is not used anymore (should be removed one day). See ##1##
    // references in the code for other parts of the code which is related.
    //
    // /// Create a hash map with base units as keys and exponents as value.
    // ///
    // /// For a base unit, it will return a single key hashmap, e.g. for meters: `{m :1}`.
    // ///
    // /// But for derived units, it will return the hashmap corresponding to its equivalent unit.
    // pub fn to_hash_map(&self) -> HashMap<String, Float> {
    //     match &self.equivalence {
    //         Some(unit) => unit.to_hash_map(),
    //         None => {
    //             let mut m = HashMap::new();
    //             m.insert(self.name.to_string(), float_parsed!(1));
    //             m
    //         }
    //     }
    // }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn quantity_is_well_linked() {
        with_test_container!(container, {
            let q = container.strong_quantity("length").unwrap();
            let u = container.strong_base_unit("km").unwrap();
            assert!(Rc::ptr_eq(q, &u.strong_quantity()));
        });
    }
}
