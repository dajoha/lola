//! Implements the [`Quantity`] struct.

use std::rc::{Rc, Weak};

use crate::value::BaseUnit;

/// Represents a "physical" quantity, like `length`, `time`, `mass`....
///
/// Each quantity have several units which can express it (e.g. for `length`: meters, kilometers,
/// miles).
///
/// Each unit of a given quantity has a weight which determines the ratio it has to be scaled,
/// compared to the other units (e.g. `1km` is 1000 times larger than `1m`).
#[derive(Debug)]
pub struct Quantity {
    name: String,
    base_units: Vec<Weak<BaseUnit>>,
}

impl Quantity {
    /// Creates a new [`Quantity`] instance.
    pub fn new(name: &str) -> Self {
        Self {
            name: name.to_string(),
            base_units: vec![],
        }
    }

    /// Used to build the [`Quantity`] instance: add a new base unit.
    pub fn add_base_unit(&mut self, base_unit: &Rc<BaseUnit>) {
        self.base_units.push(Rc::downgrade(base_unit));
    }

    /// Getter for `name`
    pub fn name(&self) -> &str {
        &self.name
    }
}

// impl Hash for Quantity {
//     fn hash<H: Hasher>(&self, state: &mut H) {
//         self.name.hash(state);
//     }
// }
