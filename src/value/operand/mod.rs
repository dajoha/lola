//! Structures which implement the [`Operand`] trait.
//!
//! [`Operand`]: crate::value::Operand

pub mod array_operand;
pub mod boolean_operand;
pub mod dict_operand;
pub mod func_operand;
pub mod num_operand;
pub mod int_operand;
pub mod str_operand;
pub mod void_operand;

pub use array_operand::*;
pub use boolean_operand::*;
pub use dict_operand::*;
pub use func_operand::*;
pub use num_operand::*;
pub use int_operand::*;
pub use str_operand::*;
pub use void_operand::*;

use std::fmt;

use crate::value::{
    IntoValue,
    Value,
    OperationResult,
    ValueMergeStrategy,
    ValueIterator,
    OperationError,
    Unit,
    Format,
};

/// Implemented by [`NumOperand`], [`IntOperand`], [`StrOperand`], [`BooleanOperand`],
/// [`FuncOperand`], [`ArrayOperand`], [`VoidOperand`].
///
/// Each of these structs can implement or not the operations proposed by the trait.
///
/// [`NumOperand`]: crate::value::NumOperand
/// [`IntOperand`]: crate::value::IntOperand
/// [`StrOperand`]: crate::value::StrOperand
/// [`BooleanOperand`]: crate::value::BooleanOperand
/// [`FuncOperand`]: crate::value::FuncOperand
/// [`ArrayOperand`]: crate::value::ArrayOperand
/// [`VoidOperand`]: crate::value::VoidOperand
pub trait Operand: fmt::Display + fmt::Debug + Format + IntoValue + ToStrOperand {
    /// Performs the `+` operation.
    fn add(&self, rhs: &Value) -> OperationResult {
        if rhs.is_str() {
            // Delegate string concatenation to StrOperand:
            let lhs = self.to_str_operand().into_value();
            lhs.add(rhs)
        } else {
            Err(err!(Operation:Unavailable))
        }
    }

    /// Performs the `-` operation.
    fn sub(&self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `*` operation.
    fn mul(&self, _: &Value, _: ValueMergeStrategy) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `/` operation.
    fn div(&self, _: &Value, _: ValueMergeStrategy) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `%` operation.
    fn modulo(&self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `^` operation.
    fn pow(&self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `==` operation.
    fn equ(&self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `<` operation.
    fn cmp_lt(&self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `>` operation.
    fn cmp_gt(&self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `<=` operation.
    fn cmp_lte(&self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `>=` operation.
    fn cmp_gte(&self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `!=` operation. Every type of operand (numbers, strings...) which have
    /// implemented `equ()` will have a default implementation for it.
    fn nequ(&self, v: &Value) -> OperationResult {
        self.equ(v).and_then(|result| match result {
            Value::Boolean(b) => Ok(b.negate().into_value()),
            _ => Err(err!(Operation:Unavailable))
        })
    }

    /// Performs the `~` operation.
    fn convert_to_unit(&self, _: &Unit) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `~!` operation.
    fn raw_convert_to_unit(&self, _: &Unit) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `&&` operation.
    fn and(&self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `||` operation.
    fn or(&self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the unary `!` operation.
    fn not(&self) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the postfix `++` operation.
    fn postfix_incr(&mut self) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the postfix `--` operation.
    fn postfix_decr(&mut self) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the prefix `++` operation.
    fn prefix_incr(&mut self) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the prefix `--` operation.
    fn prefix_decr(&mut self) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `+=` operation.
    fn set_add(&mut self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `-=` operation.
    fn set_sub(&mut self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `*=` operation.
    fn set_mul(&mut self, _: &Value, _: ValueMergeStrategy) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `/=` operation.
    fn set_div(&mut self, _: &Value, _: ValueMergeStrategy) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Performs the `[]` operation.
    fn index(&self, _: &Value) -> OperationResult {
        Err(err!(Operation:Unavailable))
    }

    /// Calls the given closure by giving to it a mutable reference to the array element indexed by
    /// the given value (value must be `int`).
    fn get_indexed_mut(
        &self,
        _index: &Value,
        _f: &dyn Fn(&mut Value) -> OperationResult
    ) -> Result<OperationResult, OperationError>
    {
        Err(err!(Operation:Unavailable))
    }

    /// Returns an iterator over the value.
    fn iterate(&self) -> Result<ValueIterator, OperationError> {
        Err(err!(Operation:NotIterable))
    }
}
