//! Implements the [`FuncOperand`] struct.

use std::fmt;

use getset::Getters;

use crate::value::{
    Value,
    Operand,
    IntoValue,
    Format,
    FormatConfig,
};
use crate::eval::Function;

/// A function operand.
#[derive(Debug, Clone, Getters)]
pub struct FuncOperand {
    #[getset(get = "pub")]
    func: Function,
}

impl FuncOperand {
    /// Creates a new function operand.
    pub fn new(func: Function) -> Self {
        Self {
            func,
        }
    }

    /// Returns a reference to the underlying function definition.
    pub fn val_ref(&self) -> &Function {
        &self.func
    }
}

impl Operand for FuncOperand {}

impl PartialEq for FuncOperand {
    fn eq(&self, other: &Self) -> bool {
        self.func.name() == other.func.name()
    }
}

impl fmt::Display for FuncOperand {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}()", self.func.name())
    }
}

impl Format for FuncOperand {
    fn format(&self, _config: &FormatConfig, _as_expr: bool) -> String {
        format!( "{}()", self.func.name())
    }
}

impl IntoValue for FuncOperand {
    fn into_value(self) -> Value {
        Value::Func(self)
    }
}

#[cfg(test)]
mod tests {
}
