//! Implements the [`IntOperand`] struct.

use std::fmt;

use crate::value::{
    Value,
    OperationError,
    OperationResult,
    IntoOperationResult,
    Operand,
    IntoValue,
    ToStrOperand,
    NumOperand,
    ValueIterator,
    ValueMergeStrategy,
    Unit,
    Format,
    FormatConfig,
};

/// An integer operand.
#[derive(Debug, Clone, PartialEq, Eq, Default, Hash)]
pub struct IntOperand(i64);

/// The result of an operation between two [`IntOperand`] instances.
pub type IntOperationResult = Result<IntOperand, OperationError>;

/// It is nice to be able to easily convert a [`IntOperationResult`] into the more generic type
/// [`OperationResult`].
impl IntoOperationResult for IntOperationResult {
    fn into_operation_result(self) -> OperationResult {
        self.map(|int_operand| int_operand.into_value())
    }
}

impl IntOperand {
    /// Creates a new [`IntOperand`] instance.
    pub fn new(value: i64) -> IntOperand {
        IntOperand(value)
    }

    /// Returns the underlying data.
    #[inline]
    pub fn val(&self) -> i64 {
        self.0
    }

    /// Checks if the two integers are equal.
    pub fn equal(&self, rhs: &IntOperand) -> bool {
        self.0 == rhs.0
    }

    /// Checks if self is lower than the rhs.
    pub fn lt_int(&self, rhs: &IntOperand) -> bool {
        self.0 < rhs.0
    }

    /// Checks if self is greater than the rhs.
    pub fn gt_int(&self, rhs: &IntOperand) -> bool {
        self.0 > rhs.0
    }

    /// Checks if self is lower than or equal to the rhs.
    pub fn lte_int(&self, rhs: &IntOperand) -> bool {
        self.0 <= rhs.0
    }

    /// Checks if self is greater thant or equal to the rhs.
    pub fn gte_int(&self, rhs: &IntOperand) -> bool {
        self.0 >= rhs.0
    }

    /// Creates a new [`IntOperand`] instance which is the result of the addition with another
    /// integer.
    pub fn add_int(&self, rhs: &IntOperand) -> IntOperationResult {
        Self::oper_to_result(self.0.checked_add(rhs.0))
    }

    /// Creates a new [`IntOperand`] instance which is the result of the subtraction with another
    /// integer.
    pub fn sub_int(&self, rhs: &IntOperand) -> IntOperationResult {
        Self::oper_to_result(self.0.checked_sub(rhs.0))
    }

    /// Creates a new [`IntOperand`] instance which is the result of the multiplication with another
    /// integer.
    pub fn mul_int(&self, rhs: &IntOperand) -> IntOperationResult {
        Self::oper_to_result(self.0.checked_mul(rhs.0))
    }

    /// Creates a new integer which is raised to the given power.
    pub fn pow_int(&self, rhs: &IntOperand) -> IntOperationResult {
        if rhs.0 < 0 {
            Err(err!(Operation:MismatchValue))
        } else {
            Self::oper_to_result(self.0.checked_pow(rhs.0 as u32))
        }
    }

    /// Creates a new [`IntOperand`] instance which is the result of the modulo (non-negative
    /// remainder) of another integer.
    pub fn modulo(&self, rhs: &IntOperand) -> IntOperationResult {
        Self::oper_to_result(self.0.checked_rem_euclid(rhs.0))
    }

    /// Common code used by many operations.
    fn oper_to_result(oper: Option<i64>) -> IntOperationResult {
        match oper {
            Some(n) => Ok(IntOperand(n)),
            None    => Err(OperationError::ValueOutOfBounds),
        }
    }

    /// Helps to implement the trait [`Operand`] without repeating the same code multiple times.
    fn int_oper(rhs: &Value, func: impl Fn(&IntOperand) -> IntOperationResult) -> OperationResult {
        match rhs {
            Value::Int(rhs) => func(rhs).into_operation_result(),
            _ => Err(err!(Operation:MismatchTypes)),
        }
    }

    /// Performs the `+=` operation.
    fn set_oper(&mut self, rhs: &Value, func: impl Fn(i64) -> Option<i64>) -> OperationResult {
        match rhs {
            Value::Int(rhs) => {
                if let Some(n) = func(rhs.0) {
                    self.0 = n;
                    Ok(self.clone()).into_operation_result()
                } else {
                    Err(OperationError::ValueOutOfBounds)
                }
            }
            _ => Err(err!(Operation:MismatchTypes)),
        }
    }

    /// Helps to implement the trait [`Operand`] without repeating the same code multiple times.
    fn num_compare(&self, rhs: &Value, func: impl Fn(&IntOperand) -> bool) -> OperationResult {
        match rhs {
            Value::Int(rhs) => Ok(boolean!(func(rhs))),
            _ => Err(err!(Operation:MismatchTypes)),
        }
    }
}

impl Operand for IntOperand {
    /// Performs the `+` operation.
    fn add(&self, rhs: &Value) -> OperationResult {
        match rhs {
            Value::Str(_) => {
                let lhs = self.to_str_operand().into_value();
                lhs.add(rhs)
            },
            Value::Num(_) => {
                let lhs = NumOperand::from(self).into_value();
                lhs.add(rhs)
            },
            _ => {
                Self::int_oper(rhs, |nv| self.add_int(nv))
            },
        }
    }

    /// Performs the `-` operation.
    fn sub(&self, rhs: &Value) -> OperationResult {
        Self::int_oper(rhs, |nv| self.sub_int(nv))
    }

    /// Performs the `*` operation.
    fn mul(&self, rhs: &Value, _: ValueMergeStrategy) -> OperationResult {
        Self::int_oper(rhs, |nv| self.mul_int(nv))
    }

    /// Performs the `/` operation.
    fn div(&self, rhs: &Value, strategy: ValueMergeStrategy) -> OperationResult {
        // Self::int_oper(rhs, |nv| self.div_int(nv))
        match rhs {
            Value::Int(rhs) => {
                let lhs = NumOperand::from(self.0);
                let rhs = NumOperand::from(rhs.0);
                lhs.div_num(&rhs, strategy).into_operation_result()
            },
            _ => Err(err!(Operation:MismatchTypes)),
        }
    }

    /// Performs the `%` operation.
    fn modulo(&self, rhs: &Value) -> OperationResult {
        Self::int_oper(rhs, |nv| self.modulo(nv))
    }

    /// Performs the `^` operation.
    fn pow(&self, rhs: &Value) -> OperationResult {
        Self::int_oper(rhs, |nv| self.pow_int(nv))
    }

    /// Performs the `==` operation.
    fn equ(&self, rhs: &Value) -> OperationResult {
        self.num_compare(rhs, |nv| self.eq(nv))
            .or_else(|_| Ok(v_false!()))
    }

    /// Performs the `<` operation.
    fn cmp_lt(&self, rhs: &Value) -> OperationResult {
        self.num_compare(rhs, |nv| self.lt_int(nv))
    }

    /// Performs the `>` operation.
    fn cmp_gt(&self, rhs: &Value) -> OperationResult {
        self.num_compare(rhs, |nv| self.gt_int(nv))
    }

    /// Performs the `<=` operation.
    fn cmp_lte(&self, rhs: &Value) -> OperationResult {
        self.num_compare(rhs, |nv| self.lte_int(nv))
    }

    /// Performs the `>=` operation.
    fn cmp_gte(&self, rhs: &Value) -> OperationResult {
        self.num_compare(rhs, |nv| self.gte_int(nv))
    }

    /// Performs the postfix `++` operation.
    fn postfix_incr(&mut self) -> OperationResult {
        let ret = self.clone();
        self.0 += 1;
        Ok(ret).into_operation_result()
    }

    /// Performs the postfix `--` operation.
    fn postfix_decr(&mut self) -> OperationResult {
        let ret = self.clone();
        self.0 -= 1;
        Ok(ret).into_operation_result()
    }

    /// Performs the prefix `++` operation.
    fn prefix_incr(&mut self) -> OperationResult {
        self.0 += 1;
        Ok(self.clone()).into_operation_result()
    }

    /// Performs the prefix `--` operation.
    fn prefix_decr(&mut self) -> OperationResult {
        self.0 -= 1;
        Ok(self.clone()).into_operation_result()
    }

    /// Performs the `+=` operation.
    fn set_add(&mut self, rhs: &Value) -> OperationResult {
        let lhs = self.0;
        self.set_oper(rhs, |rhs| lhs.checked_add(rhs))
    }

    /// Performs the `-=` operation.
    fn set_sub(&mut self, rhs: &Value) -> OperationResult {
        let lhs = self.0;
        self.set_oper(rhs, |rhs| lhs.checked_sub(rhs))
    }

    /// Performs the `*=` operation.
    fn set_mul(&mut self, rhs: &Value, _: ValueMergeStrategy) -> OperationResult {
        let lhs = self.0;
        self.set_oper(rhs, |rhs| lhs.checked_mul(rhs))
    }

    /// Performs the `/=` operation.
    fn set_div(&mut self, rhs: &Value, _: ValueMergeStrategy) -> OperationResult {
        let lhs = self.0;
        match rhs {
            Value::Int(IntOperand(0)) => {
                Err(OperationError::DivideByZero)
            }
            Value::Int(rhs) => {
                if let Some(n) = lhs.checked_div(rhs.0) {
                    self.0 = n;
                    Ok(self.clone()).into_operation_result()
                } else {
                    Err(OperationError::ValueOutOfBounds)
                }
            }
            _ => Err(err!(Operation:MismatchTypes)),
        }
    }

    /// Performs the `~!` operation.
    fn raw_convert_to_unit(&self, unit: &Unit) -> OperationResult {
        let num = NumOperand::from(self);
        num.raw_convert_num_to_unit(unit).into_operation_result()
    }

    /// Returns an iterator over the value.
    fn iterate(&self) -> Result<ValueIterator, OperationError> {
        if self.0 < 0 {
            return Err(err!(Operation:MismatchValue));
        }
        let iter = (0..self.0)
            .map(|i| IntOperand(i).into_value());

        Ok(ValueIterator::new(Box::new(iter)))
    }
}

impl fmt::Display for IntOperand {
    /// Displays a formatted version of a number.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Format for IntOperand {
    fn format(&self, _config: &FormatConfig, _as_expr: bool) -> String {
        format!( "{}", self.0)
    }
}

impl IntoValue for IntOperand {
    fn into_value(self) -> Value {
        Value::Int(self)
    }
}

impl From<i32> for IntOperand {
    fn from(v: i32) -> IntOperand {
        IntOperand(v as i64)
    }
}

impl From<usize> for IntOperand {
    fn from(v: usize) -> IntOperand {
        IntOperand(v as i64)
    }
}

impl From<i64> for IntOperand {
    fn from(v: i64) -> IntOperand {
        IntOperand(v)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn basic_add() {
        let (a, b) = ( int!(1), int!(4) );
        assert_eq!(a.add(&b).unwrap(), int!(5));
    }

    #[test]
    fn int_with_num_operation() {
        with_test_container!(container, {
            let (a, b) = ( int!(1), number!(container, 4) );
            assert_eq!(a.add(&b).unwrap(), number!(container, 5));
        })
    }

    #[test]
    fn num_with_int_operation() {
        with_test_container!(container, {
            let (a, b) = ( number!(container, 1), int!(4) );
            assert_eq!(a.add(&b).unwrap(), number!(container, 5));
        })
    }
}
