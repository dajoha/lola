//! Implements runtime temperature functions.
//!
//! * `to_fahrenheit()`
//! * `to_celsius()`
//! * `to_kelvin()`

use core::ops::{MulAssign, AddAssign, SubAssign};

use rug::float::Round;
use rug::ops::{MulAssignRound, AddAssignRound, SubAssignRound};

use crate::context::{Container, Scope};
use crate::value::{Value, NumOperand, IntoValue};
use crate::eval::ExprResult;
use crate::{num_operand, unit, compound_quantity, err};
use crate::eval::{
    Function,
    arg_checker as arg,
    func_macros::*,
};

use TemperatureUnit::{
    Kelvin,
    Celsius,
    Fahrenheit,
    Invalid,
};

/// A temperature unit.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum TemperatureUnit {
    Kelvin,
    Celsius,
    Fahrenheit,
    Invalid,
}

/// Detects which temperature unit the given [`NumOperand`] has.
fn get_temperature_unit(num: &NumOperand, container: &Container) -> TemperatureUnit {
    // TODO: make these variables static:
    let celsius    = unit!(container, ("°C"):1);
    let kelvin     = unit!(container, ( "K"):1);
    let fahrenheit = unit!(container, ("°F"):1);

    match num.unit() {
        u if u == &celsius    => Celsius,
        u if u == &kelvin     => Kelvin,
        u if u == &fahrenheit => Fahrenheit,
        _                     => Invalid,
    }
}

/// Converts the given temperature value to another temperature unit.
pub fn convert_temperature(value: &Value, to_unit: TemperatureUnit, container: &Container) -> ExprResult {
    let num = value.get_num().ok_or(err!(Expr:Call:ArgList:MismatchTypes))?;
    let from_unit = get_temperature_unit(num, container);

    if from_unit == to_unit {
        return Ok(value.clone());
    }

    let unit = match to_unit {
        Fahrenheit => "°F",
        Celsius    => "°C",
        Kelvin     => "K",
        _ => return Err(err!(Expr:Call:ArgList:MismatchUnits)),
    };

    let mut num = num.num().clone();

    macro_rules! f {
        (9/5) => (float_parsed!(1.8));
        (5/9) => (float!(5) / float!(9));
        (459.67) => (float_parsed!(459.67));
        (273.15) => (float_parsed!(273.15));
    }

    match ( from_unit, to_unit ) {
        ( Celsius,    Fahrenheit ) => {
            // num * 9/5 + 32
            num.mul_assign(f!(9/5));
            num.add_assign_round(32, Round::Up);
        },
        ( Kelvin,     Fahrenheit ) => {
            // num * 9/5 - 459.67
            num.mul_assign(f!(9/5));
            num.sub_assign_round(f!(459.67), Round::Up);
        },
        ( Fahrenheit, Celsius )    => {
            // (num - 32) * 5/9
            num.sub_assign(32);
            num.mul_assign_round(f!(5/9), Round::Up);
        },
        ( Kelvin,     Celsius )    => {
            // num - 273.15
            num.sub_assign_round(f!(273.15), Round::Up);
        },
        ( Fahrenheit, Kelvin )     => {
            // (num + 459.67) * 5/9
            num.add_assign(f!(459.67));
            num.mul_assign_round(f!(5/9), Round::Up);
        },
        ( Celsius,    Kelvin )     => {
            // num + 273.15
            num.add_assign_round(f!(273.15), Round::Up);
        },
        _ => return Err(err!(Expr:Call:ArgList:MismatchUnits)),
    };
    Ok(num_operand!(container, [expr] (num) (unit):1).into_value())
}

/// Creates a new temperature conversion function.
fn temperature_conversion_function(name: &str, to_unit: TemperatureUnit) -> Function {
    func!(
        (name),
        args![
            t: arg::has_quantity(compound_quantity!{ temperature: 1 }),
        ],
        body!(move |values, cxt| {
            convert_temperature(&values[0], to_unit, cxt.container)
        })
    )
}

/// Creates temperature functions in the given scope.
pub fn create_temperature_functions(scope: &mut Scope) {
    // Temperature functions:
    let temperature_functions = [
        ( "to_fahrenheit", TemperatureUnit::Fahrenheit ),
        ( "to_celsius",    TemperatureUnit::Celsius    ),
        ( "to_kelvin",     TemperatureUnit::Kelvin     ),
    ];
    for ( name, unit ) in temperature_functions.iter() {
        scope.add_named_function(temperature_conversion_function(name, *unit)).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::numbers::approx_eq;

    macro_rules! temp_val {
        ($v:expr, Celsius,    $container:expr) => ( number!($container, [expr] ($v) ("°C"):1) );
        ($v:expr, Fahrenheit, $container:expr) => ( number!($container, [expr] ($v) ("°F"):1) );
        ($v:expr, Kelvin,     $container:expr) => ( number!($container, [expr] ($v) ( "K"):1) );
        // For error testing, creates a value in kilometers:
        ($v:expr, km,         $container:expr) => ( number!($container, [expr] ($v) ("km"):1) );
    }
    macro_rules! test_conv {
        ($v1:expr, $u1:ident, $v2:expr, $u2:ident) => {
            with_test_context!(cxt, {
                let a = temp_val!(float_parsed!($v1), $u1, cxt.container);
                let b = convert_temperature(&a, $u2, cxt.container).unwrap();
                let b_float = b.unwrap_num().num();
                let v2_float = float_parsed!($v2);
                assert!(approx_eq(b_float, &v2_float));
            })
        };
    }
    macro_rules! test_err {
        ($v1:expr, $u1:ident, $u2:ident, $err:expr) => {
            with_test_context!(cxt, {
                let a = temp_val!(float_parsed!($v1), $u1, cxt.container);
                let r = convert_temperature(&a, $u2, cxt.container);
                assert_eq!(r, $err);
            })
        };
        ($str:expr, $u:ident, $err:expr) => {
            with_test_context!(cxt, {
                let a = string!($str);
                let r = convert_temperature(&a, $u, cxt.container);
                assert_eq!(r, $err);
            })
        };
    }

    #[test]
    fn celsius_to_celsius() {
        test_conv!(0, Celsius, 0, Celsius);
        test_conv!(4, Celsius, 4, Celsius);
    }

    #[test]
    fn kelvin_to_kelvin() {
        test_conv!(0, Kelvin, 0, Kelvin);
        test_conv!(100, Kelvin, 100, Kelvin);
    }

    #[test]
    fn fahrenheit_to_fahrenheit() {
        test_conv!(32, Fahrenheit, 32, Fahrenheit);
        test_conv!(39.2, Fahrenheit, 39.2, Fahrenheit);
    }

    #[test]
    fn celsius_to_fahrenheit() {
        test_conv!(0, Celsius, 32, Fahrenheit);
        test_conv!(4, Celsius, 39.2, Fahrenheit);
    }

    #[test]
    fn kelvin_to_fahrenheit() {
        test_conv!(0, Kelvin, -459.67, Fahrenheit);
        test_conv!(100, Kelvin, -279.67, Fahrenheit);
    }

    #[test]
    fn fahrenheit_to_celsius() {
        test_conv!(32, Fahrenheit, 0, Celsius);
        test_conv!(39.2, Fahrenheit, 4, Celsius);
    }

    #[test]
    fn kelvin_to_celsius() {
        test_conv!(0, Kelvin, -273.15, Celsius);
        test_conv!(100, Kelvin, -173.15, Celsius);
    }

    #[test]
    fn fahrenheit_to_kelvin() {
        test_conv!(440.33, Fahrenheit, 500, Kelvin);
        test_conv!(-297.67, Fahrenheit, 90, Kelvin);
    }

    #[test]
    fn celsius_to_kelvin() {
        test_conv!(0, Celsius, 273.15, Kelvin);
        test_conv!(100, Celsius, 373.15, Kelvin);
    }

    #[test]
    fn mismatch_values() {
        test_err!(4, km, Celsius, Err(err!(Expr:Call:ArgList:MismatchUnits)));
        test_err!("A string", Kelvin, Err(err!(Expr:Call:ArgList:MismatchTypes)));
    }
}
