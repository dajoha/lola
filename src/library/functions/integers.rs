//! Implements runtime integers functions.
//!
//! * `div_int()`

use crate::context::Scope;

use crate::eval::{
    arg_list_checker as arg_list,
    arg_checker as arg,
    func_macros::*,
};

/// Creates functions dealing with integers in a container.
pub fn create_integer_functions(scope: &mut Scope) {
    macro_rules! add_func {
        ($($tt:tt)*) => { scope.add_named_function(func!($($tt)*)).unwrap() }
    }

    // Performs an integer division:
    add_func!(
        div_int,
        args![
            lhs: arg::is_int,
            rhs: arg::is_int,
        ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, _| {
            let lhs = args[0].unwrap_int();
            let rhs = args[1].unwrap_int();
            if rhs.val() == 0 {
                Err(err!(Expr:Operation:DivideByZero))
            } else {
                let value = lhs.val().checked_div(rhs.val())
                    .ok_or(err!(Expr:Operation:ValueOutOfBounds))?;
                Ok(int!(value))
            }
        })
    );
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_div_int() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, r#" div_int(6, 2) "#), int!(3));
            assert_eq!(eval_err!(cxt, r#" div_int(6, 0) "#), err!(Expr:Operation:DivideByZero));
            assert_eq!(eval_err!(
                cxt,
                r#" div_int(-9223372036854775808, -1) "#),
                err!(Expr:Operation:ValueOutOfBounds
            ));
        })
    }
}
