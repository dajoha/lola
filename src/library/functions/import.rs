//! Implements the `import()` runtime function.

use crate::context::Scope;
use crate::eval::{
    function::FunctionBody,
    ExprError,
    arg_checker as arg,
    func_macros::*,
};
use crate::parse::{parse_expr, Span};
use crate::value::Value;
use crate::library::ContextRef;

/// Creates the `import()` function in the given scope.
pub fn create_import_function(scope: &mut Scope) {
    macro_rules! add_func {
        ($($tt:tt)*) => { scope.add_named_function(func!($($tt)*)).unwrap() }
    }

    let import_closure = |args: &[Value], cxt: &mut ContextRef, caller_span: &Span| {
        let path = args[0].unwrap_str().val_ref();
        let caller_source = caller_span.parsed_input_index.map(|index|
            cxt.container.get_parsed_input_source(index)
        );
        let import = {
            let importer = cxt.container.importer().borrow();
            importer.get_code(path, caller_source)
                .map_err(|err| ExprError::IO(err))?
        };
        match parse_expr(&import.0, cxt.container, Some((path.to_owned(), import.1))) {
            Ok(expr) => Ok(expr),
            Err(err) => Err(ExprError::Parse(path.to_owned(), err)),
        }
    };

    add_func!(
        import,
        args![ file: arg::is_string ],
        body_eval!(FunctionBody::NativeToEval(Box::new(import_closure))),
    );
}
