//! A library to parse and run code written with the lola scripting language.
//!
//! # Basic usage example:
//!
//! ```
//! use lola::context::ContextRef;
//! use lola::library::create_prefilled_context;
//! use lola::parse::parse_expr;
//!
//! let lola_code = "max(400m, 20km, 2000cm)";
//!
//! // Create a context where we can run the code:
//! let (mut container, mut scope) = create_prefilled_context();
//! let mut cxt = ContextRef::new(&mut container, &mut scope);
//!
//! // Parse the expression:
//! let expr = match parse_expr(lola_code, cxt.container, None) {
//!     Ok(expr) => expr,
//!     Err(err) => {
//!         println!("Oops, a parsing error occured: {:?}", err);
//!         return;
//!     },
//! };
//!
//! // Evaluate the code and print the result:
//! match expr.eval(&mut cxt) {
//!     Ok(value) => println!("Result: {}", value),
//!     Err(err) => println!("Error: {}", err),
//! }
//! ```
//!
//! The above example will output:
//!
//! ```none
//! Result: 20 km
//! ```

#[macro_use] pub mod util;
pub mod value;
pub mod context;
pub mod eval;
pub mod parse;
pub mod library;

use util::numbers;
