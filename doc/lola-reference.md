# Lola - Language reference

This document explains the main syntax of the Lola language.

# Types

Values in the lola language can have 7 different types:

 * `number` - an arbitrary precision float number, with a physical unit
 * `int` - a system integer number (64 bits, without any physical unit)
 * `boolean` - can be `true` or `false`
 * `string` - a string of UTF-8 characters
 * `array` - an array of values: mixed types are allowed
 * `function` - a callable function
 * `void` - a void value

The `number` type is special, because it also brings a *quantity* with it.  
E.g: a certain value could represent a distance, then its whole type would be `number(length)`, or
shorter: `{length}`.

## The `number` type

The numeric value of a `number` is an arbitrary precision float number (actually the precision has a
fixed size of 128 bits, but this limit has been arbitrarily choosen, and may become configurable in
future versions).

Each value having the type `number` has a *unit* (see the part about units below).

E.g.:
 * the value `2 km` is a `number` having the `km` unit (kilometers)
 * the value `3h` is a `number` having the `h` unit (hours)
 * the value `45.0` is a `number` having an empty unit (no unit)

Like in Rust, underscores can be added after the first digit to improve readabiliy for big numbers,
e.g. `12_000_000 km` is equivalent to `12000000 km`.

## The `int` type

An `int` is any numeric value which is created with not decimal point, exponent or unit. E.g.: `12`,
`-45`. It is internally represented as a signed 64 bits integer value, allowing a range of values
from `-9223372036854775808` to `9223372036854775807`.

Like in Rust, underscores can be added after the first digit to improve readabiliy for big numbers,
e.g. `12_000_000` is equivalent to `12000000`.

### Automatic conversion from `int` to `number`

When using an `int` in the place where a `number` is expected, `int` values are automatically
converted to `number`s. E.g.:

* `sin(1)` will first convert the `int` value `1` to the number `1.0`, then perform the operation
  `sin(1.0)`.
* `1 + 2.0` will first convert the `int` value `1` to the number `1.0`, then perform the operation
  `1.0 + 2.0`.

## The `boolean` type

A `boolean` value can have two states: `true` or `false`.

* It is the result type of comparison operations (`==`, `<` ...);
* It is the expected type for the condition expression in a `if <CONDITION> {...}` block or a `while
  <CONDITION> {...}` block.

```
>> 2 == 3
false
>> if true { print("Ok") }
Ok
```

`boolean` values can be manipulated with the help of boolean operators:

```
>> 30 > 20 && 100 < 200
true
>> !true || !false
true
```

## The `string` type

`string` values can hold any sequence of UTF-8 characters, e.g.:

* `"hello"`
* `"α β Δ"`
* `""` (empty string)

They can be compared together with the `==` operator, and concatenated with the `+` operator, e.g.:

```
>> "foo" + "bar" == "foobar"
true
```

Double quotes can be added in a string by escaping them with a backslash `\`:

```
>> print("A \"message\" with quotes")
A "message" with quotes
```

## The `array` type

An array is a list of values, grouped together into a single value. The syntax is the following:

```
[ <VALUE1>, <VALUE2>, ... ]
```

E.g.:

* `[ 1, 2, 3 ]`: an array of three integers;
* `[ "foo", 2km ]`: values can have different types;
* `[ [1,2], [3,4] ]`: arrays can be nested;
* `[]`: an empty array.

### Accessing an array (indexing)

An array element of the given index can be accessed using the following syntax:

```
ARRAY[INDEX]
```

Indexing starts from 0.

E.g.:

```
>> my_array = [ "foo", "bar", "hi" ]
[ foo, bar, hi ]
>> my_array[1]
bar
```

### Looping over an array

You can loop over the different elements of an array by using the `for` keyword:

```
>> for n in [ 12, 23, 45 ] { print("Value", n) }
Value 12
Value 23
Value 45
```

## The `function` type

Every defined function has the `function` type. `function` values can be used like any other
variables: the can be stored in arrays, passed as arguments in functions...

```
>> funcs = [ min, max ]
[ min(), max() ]
>> funcs[0](12, 30, 80)
12
>> funcs[1](12, 30, 80)
80
```

## The `void` type

A `void` value has no real information stored into it. This is what functions return when they don't
use the `return` keyword *and the last expression has an ending semicolon*.

`void` values can be explicitly created by using `()` or `{}`:

```
>> print() == ()
true
>> type( () )
void
>> () == {}
true
```

# Units and quantities

## Units

The main feature that could distinguish Lola from other scripting languages is the native handling
of *SI units*.

Units can be separated or not with spaces from their number.

E.g.:
 * `2km` would be interpreted as a `{length}` of 2 kilometers;
 * `2 km` would be interpreted as a `{length}` of 2 kilometers;
 * `1.2h` would be interpreted as a `{time}` of 1.2 hours;

Here is a partial list of the actually recognized units, grouped by quantity (see Appendix for a
complete list):

 * Length: `um`, `mm`, `cm`, `dm`, `m`, `km`, `ft`, `in`, `mi`, `au`, `ly`, `pc`
 * Area: `ha`
 * Volume: `l`, `cl`, `ml`,
 * Mass: `ng`, `μg`, `ug`, `mg`, `g`, `kg`, `t`
 * Time: `ns`, `µs`, `us`, `ms`, `s`, `min`, `h`, `d`, `y`
 * Speed: `nd`, `c`
 * Force: `N`

## Unit exponents

A unit can have an exponent. The exponent must be placed just after the unit, without space.

E.g.:
 * "3 square meters" can be expressed as `3 m2`

## Compound units

Basic units like `km` (kilometers), `s` (seconds) can be compound by separating them with the
characters `.` or `/`, without space:

 * The character `.` means a multiplication;
 * The character `/` means a division.

E.g.:
 * `3km/h`, `3km.h-1` both mean "three kilometers per hour";
 * `2kW.h` means "two kiloWatt-hour".

## Quantities

A *quantity* represents a physical concept, like **length**, **time**, etc..., or any compound of
these concepts (like `length/time` which can represent **speed**),  which can be
expressed with different *units*.

Here is a list of some of the recognized quantities (See Appendix for a complete list):

 * `length`
 * `area`
 * `volume`
 * `mass`
 * `time`
 * `speed`
 * `force`

Every *unit* or *compound unit* has a quantity, e.g.:

 * `km/h` or `m/s` have the quantity `length/time`
 * `W`, `kW` or `MW` have the quantity `power`
 * `kHz` or `s-1` have the quantity `time-1`

# Operations

Many operations can be performed on values.

## Arithmetic operations

The following operations are available:

 * `a + b`
 * `a - b`
 * `a * b`
 * `a / b`
 * `a % b`
 * `a ^ b`

## Assignment operations

 * `a = b`
 * `a += b`
 * `a -= b`
 * `a *= b`
 * `a /= b`
 * `a++`
 * `a--`
 * `++a`
 * `--a`

## Comparison operations

 * `a == b`
 * `a != b`
 * `a <  b`
 * `a <= b`
 * `a >  b`
 * `a >= b`

### Numbers, and addition-family / comparison operations

When performing an addition, a subtraction or a comparison between two numbers, the quantities of
the values must be equivalent. E.g., you can add meters with kilometers (`{length}` + `{length}`),
but not hours with kilometers (`{time}` + `{length}`):

```
>> 2km + 200m
2.2 km
>> 10 m/s + 36 km/h
20 m.s-1
>> 2km + 2h
line 1, column 1: Operation: Mismatch units
>> 2km > 1500m
true
>> 2km > 1h
line 1, column 1: Operation: Mismatch units
```

### Numbers, and multiplication-family operations

When multiplying two values, their quantities are compound. E.g.:

```
>> dist = 2km
>> time = 0.2h
>> dist / time
10 km.h-1
```

### Numbers, and power-family operations

When raising a value to a given power, their quantities are adjusted accordingly. E.g.:

```
>> 2km ^ 2
4 km2
```

## Boolean operations

 * `a && b`
 * `a || b`
 * `!a`

## Unit conversion operations

### Normal unit conversion

It is possible to convert a value into a compatible unit. Two units are compatible if they express
the same quantity (e.g.: `km/h` and `mi/s`).

Here is the syntax:

```
VALUE ~NEW_UNIT
```

Spaces are not allowed between the `~` operator and `NEW_UNIT`.

#### Example: converting a value from km/h to m/s:

```
>> 36 km/h ~m/s
10 m.s-1
```

### Raw unit conversion

Sometimes it is useful to change the unit of a number without taking care if the units are
compatible or not. It is possible with the raw unit conversion operator, `~!`. Here is the syntax:

```
VALUE !~NEW_UNIT
```

Examples:

```
>> 2.4km ~!s
2.4s
```

This makes it possible to strip off the unit of a value, by giving and empty unit to the operator:

```
>> 2km ~!
2
```

# Blocks, variable scopes

There are two types of blocks:

* `(...)`: a block of expressions which stay in the same scope level;
* `{...}`: a block of expressions which enters into a sub-scope.

**Important**: A block has always a *return value*:

 * If the last expression of a block has a trailing semi-colon, then the return value of the block
   will be `void`;
 * Otherwise, the return value of the block will be the resultant value of the last expression.

## Parenthese blocks

The syntax for parenthese blocks is (carriage returns are optional):

```
(
    EXPR1;
    EXPR2;
    ...
    EXPRN[;]
)
```

Actually, when using parentheses inside arithmetic expression, it uses a simple case of the
parenthese block syntax:

```
2 * ( 7 + 3 )
```

But even if it's rarely useful, it can use multiple expressions, as allowed by the syntax:

```
>> 2 * ( seven = 2; seven += 5; seven + 3 )
20
>> seven
7
```

Explanation: here, `seven` is first assigned to `2`; then it is added `5`, which stores `7` into
the variable `seven`. Then, the parenthese block returns `seven + 3`, which is equal to `7 + 3` =
`10`; finally, the result of the parenthese block (`10`) is multiplied by `2`, which gives `20`. But
the variable `seven` is still equal to `7`, because the operations `... + 3` and `2 * ...` are not
assignment operators, so they don't affect the variable `seven`.

## Curly-bracket blocks

The syntax for curly-bracket blocks is (carriage returns are optional):

```
{
    EXPR1;
    EXPR2;
    ...
    EXPRN[;]
}
```

When inside a curly-bracket block, expressions are evaluated in a new sub-level of scoping; then
if variables are created, they will be inaccessible outside of the block.

E.g.:

```
let a = 78;
{
    let a = "foo";
    let b = 12;
};
print(a);
print(b);

=========== OUTPUT ===========
78
line 7, column 7: Undefined variable: 'b'
```

```
let a = {
    let b = 70;
    b * 10
};
print(a);
print(b);

=========== OUTPUT ===========
700
line 6, column 7: Undefined variable: 'b'
```

## Leaving a curly-bracket block [unstable]

*Warning*: this language feature is unstable, and the documentation about it (just below) is not
accurate.

It is possible to leave a curly-bracket block prematurely, by using the `leave` keyword:

```
{
    print("One");
    leave;
    print("Two");
};

=========== OUTPUT ===========
One
```

The `leave` keyword can be given a return value:

```
let v = {
    let a = 11;
    if (a > 1000) leave "Big number";
    "Small number"
};
print(v);

=========== OUTPUT ===========
Small number
```

# Variables

## Defining a variable

The syntax to define a new variable is:

```
let <VARNAME> [ = <INITIAL_VALUE> ];
```

A variable can be defined only once in a given scope. If no initial value is defined, then the
variable will hold the void value, until it is modified.

### Exception: defining a global variable without `let`

In the global scope (not inside any function or curly-bracket block), a variable can also be defined
*without* the `let` keyword:

```
<VARNAME> [ = <INITIAL_VALUE> ];
```

This exception is allowed for two reasons:

 * The main goal of the `let` keyword is to define a variable in the current scope, without
   interferring with parent scopes; but in the global scope, there is no parent scope to interfer
   with.
 * When Lola is run in an interactive mode, like with `lola-cli`, it is simply unacceptable to be
   forced to enter `let a = 4` instead of `a = 4`, while there is no good reason for it, as it has
   been pointed out just above.

## Modifying an existing variable

Once a varible has been defined, it can be modified with one of the available assignment operators:

```
<VARNAME> = <NEW_VALUE>;
<VARNAME> += <VALUE_TO_ADD>;
<VARNAME> -= <VALUE_TO_SUBTRACT>;
<VARNAME> *= <VALUE_TO_MULTIPLY>;
<VARNAME> /= <VALUE_TO_DIVIDE>;
```

# Functions

## Calling a function

The syntax to call a function is pretty straightforward:

```
FUNC_NAME([ARG1, [ARG2, ....]])
```

Example:

```
>> max(2km, 1500m, 3km)
3 km
>> sin(1)
8.414709848078965066525023216302989e-1
```

## Pre-defined functions

There are many pre-defined functions, see the Appendix to see a full list.

## Defining a user function

The syntax to define a new function is the following:

```
fn <FUNCTION_NAME>([<ARG1_DEF>, [<ARG2_DEF>, ...]]) {
    <FUNCTION_BODY>
};
```

*Note*: the semicolon at the very end is *quite always mandatory* (unless you want to *return* the
function, at the end of a block). Indeed, a function definition is a statement like any other one,
then if it is followed by other statements, then it must separated with a semicolon.

The curly brackets are optional if there is only one expression.

Inside the function parentheses, an argument definition can be only a name, or it can be typed:

```
<ARG_NAME>[: <ARG_TYPE>]
```

Examples:

```
fn print_hello() {
    print("Hello");
};

fn print_something(something) {
    print(something);
};

fn print_string(something: string) {
    print(something);
};

fn is_bigger_than_earth(distance: {length}) {
    distance > 12_000 km
}
```

#### Scoping for a user-defined function

A function will be only available in the scope where it was defined (and in the child scopes, of
course).

E.g.:

```
fn foo() {
    print("FOO");
};

{ // Entering into a sub-scope...
    fn bar() {
        print("BAR");
    };
};

foo();
bar();

=========== OUTPUT ===========
FOO
line 12, column 1: Undefined variable: 'bar'
```

## Anonymous functions

There is an alternative syntax to define a function:

```
fn([<ARG1_DEF>, [<ARG2_DEF>, ...]]) {
    <FUNCTION_BODY>
};
```

This can be useful when wanting to pass a little function to another one (callback), without naming
it.

For example, here is how we can define a function which would filter an array, by testing a
predicate on each of its elements:

```
fn filter(arr: array, predicate: function) {
    let output = [];
    for element in arr {
        if predicate(element) output += element;
    };
    output
};

filtered = filter([ 4, 9, 15, 23 ], fn(n) n < 10);
print(filtered);

=========== OUTPUT ===========
[ 4, 9 ]
```

# Flow control statements

## The `if`, `if ... else` statements

A condition statement has the following syntax:

```
if <COND> <THEN_BLOCK> [else <ELSE_BLOCK>];
```

Even if it's not mandatory, it is strongly advised to add some curly brackets around `THEN_BLOCK`s
and `ELSE_BLOCK`s, for two reasons:

 * It prevents many syntax errors (mainly due to conflicts with the *unit* system);
 * It improves readability.

Once this advice has been accepted, the condition statement has the following syntax:

```
if <COND> {
    <THEN_BLOCK>
};

if <COND> {
    <THEN_BLOCK>
} else {
    <ELSE_BLOCK>
};
```

### Return value for condition statements

If/else statements can have a return value by omitting the semicolon for the last expression, e.g.:

```
let n = 87000;
let s = if n > 2000 {
    "big"
} else {
    "small"
};
print(n, "is a", s, "number.");

=========== OUTPUT ===========
87000 is a big number.
```

## Infinite loops: `loop`

Syntax:

```
loop {
    <EXPRS>
};
```

This will run `<EXPRS>` forever.

The only way to break the loop is to use the `break [VALUE]` statement, or the `return [VALUE]` when
inside a function.

E.g.:

```
n = 3;
loop {
    print("n =", n);
    if n-- == 0 { break };
};

=========== OUTPUT ===========
n = 3
n = 2
n = 1
n = 0
```

### Return value for infinite loops

An infinite loop can return a value, by giving a value to the `break` statement:

```
let n = 3;
r = loop {
    n = n ^ 2;
    if n > 1000 {
        break n;
    };
};
print("r =", r);

=========== OUTPUT ===========
r = 6561
```

## `while` loops

Syntax:

```
while <COND> <BLOCK>
```

Executes `<BLOCK>` while the given condition is met. E.g.:

```
let n = 1;
let s = "";
while len(s) < 25 {
    s += n + ",";
    n *= 2;
};
print(s);

=========== OUTPUT ===========
1,2,4,8,16,32,64,128,256,
```

A `while` loop is breakable with a `break [VALUE]` statement.

The `continue [VALUE]` statement can be used to jump directly to the next iteration.

### Return value for `while` loops

A `while` loop can have a return value, this is the last expression without trailing semicolon (or
the value of a break statement):

```
let n = 1;
let v = while n < 300 {
    n *= 2
};
print(v);

=========== OUTPUT ===========
512
```

The same but more concise:

```
let n = 1;
print(while (n < 300) n *= 2);

=========== OUTPUT ===========
512
```

## `for` loops

Syntax:

```
for <VARIABLE> in <ITERABLE> {
    <EXPRS>
};
```

Alternative syntax (`for` loop without iteration variable):

```
for <ITERABLE> {
    <EXPRS>
};
```

The first syntax works as expected:

 * it iterates over each element of `<ITERABLE>`;
 * for each loop, it copies the current element into `<VARIABLE>`, then it executes `<EXPRS>`.

The second syntax works the same way, but the iterated element is not stored in a variable.

The `break [VALUE]` statement can be used to break the loop.

The `continue [VALUE]` statement can be used to jump directly to the next iteration.

E.g:

```
let numbers = [ 45, 78, 101 ];
let sum = 0;
for num in  numbers {
    sum += num;
};
print(sum);

=========== OUTPUT ===========
224
```

### Iterable values

There are actually two types of value which can be iterated:

* arrays (iterate over each array element);
* positive integers (iterate over integers from 0 to (n-1));

E.g. with integers:

```
for i in 3 {
    print("Loop", i);
};

for 3 {
    print("Three times");
};

=========== OUTPUT ===========
Loop 0
Loop 1
Loop 2
Three times
Three times
Three times
```

### Return value

`for` loops can have a return value, this is the last expression without trailing semicolon (or the
value of a `break [VALUE]` statement:

```
let n = 1;
print(for 3 { n *= 2 });

=========== OUTPUT ===========
8
```

# Exceptions

## Throwing exceptions

The keyword `throw` allows to raise a exception. The thrown value can be of any type.

Syntax:

```
throw <VALUE>;
```

## `try` / `catch` blocks

Syntax:

```
try <TRY_BLOCK> [catch [VARNAME] <CATCH_BLOCK>]
```

If a `throw` statement is executed inside a `try` block, then the execution of the `try` block is
stopped, and the thrown value is sent to the sibling `catch` block. If there is no sibling `catch`
block, then the exception is silently skipped.

E.g.:

```
v = -4;
let square_root = try {
    if v < 0 {
        throw "v must be positive";
    };
    sqrt(v)
}
catch msg {
    print("An error was catched:", msg);
    "ERROR"
};
print("square_root =", square_root);

=========== OUTPUT ===========
An error was catched: v must be positive
square_root = ERROR
```

# Appendix

## Recognized units

Here is a list of the actually recognized units, grouped by quantity:

 * Length: `nm`, `μm`, `um`, `mm`, `cm`, `dm`, `m`, `km`, `ft`, `in`, `mi`, `au`, `ly`, `pc`
 * Area: `ha`
 * Volume: `l`, and all SI sub-units from `yl` to `Yl`
 * Mass: `t`, `g`, and all SI sub-units from `yg` to `Yg`
 * Time: `s`, `min`, `h`, `d`, `y`, and all SI sub-units from `ys` to `Ys`
 * Speed: `nd`, `c`
 * Force: `N`, and all SI sub-units from `yN` to `YN`
 * Energy: `J`, `cal`, `kcal`, `meV`, `eV`, `keV`, `MeV`, `GeV`, `TeV`, and all SI sub-units from
   `yJ` to `YJ`
 * Pression: `Pa`, `bar`, and all SI sub-units from `yPa` to `YPa`
 * Power: `W`, `hp`, and all SI sub-units from `yW` to `YW`
 * Electric current: `A`, and all SI sub-units from `yA` to `YA`
 * Electric charge: `C`, and all SI sub-units from `yC` to `YC`
 * Electric potential: `V`, and all SI sub-units from `yV` to `YV`
 * Electric resistance: `ohm`, `Ω`, and all SI sub-units from `yΩ` to `YΩ` and from `yohm` to `Yohm`
 * Electric conductance: `S`, and all SI sub-units from `yS` to `YS`
 * Electric inductance: `H`, and all SI sub-units from `yH` to `YH`
 * Electric capacitance: `F`, and all SI sub-units from `yF` to `YF`
 * Magnetic induction: `G`, `T`, and all SI sub-units from `yT` to `YT`
 * Magnetic flux: `Mx`, `Wb`
 * Frequence: `Hz`, `kHz`, `MHz`, `GHz`, `THz`, `Bq`, `GBq`
 * Amount: `mol`
 * Temperature: `K`, `°C`, `°F`, and all SI sub-units from `yK` to `YK`
 * Angle: `rad`, `°`
 * Currency: `€`, `$`
 * Computer size: `B`, `KiB`, `MiB`, `GiB`, `TiB`, `PiB`, `KB`, `MB`, `GB`, `TB`, `PB`

## Quantities

Here is a list of the actually recognized quantities:

 * `length`
 * `area`
 * `volume`
 * `mass`
 * `time`
 * `speed`
 * `force`
 * `energy`
 * `pression`
 * `power`
 * `electric-current`
 * `electric-charge`
 * `electric-potential`
 * `electric-resistance`
 * `electric-conductance`
 * `electric-inductance`
 * `electric-capacitance`
 * `magnetic-induction`
 * `magnetic-flux`
 * `frequence`
 * `amount`
 * `temperature`
 * `angle`
 * `currency`
 * `computer-size`

## Pre-defined functions

### Math functions

The following functions accept numbers with a optional unit:

    abs(n)
    ceil(n)
    floor(n)
    round(n)
    fract(n)
    trunc(n)
    sqrt(n)

The following functions accept multiple quantity-equivalent numbers:

    max(...)
    min(...)

The following functions accept numbers with a optional unit and will return a boolean:

    is_finite(n)
    is_infinite(n)
    is_integer(n)
    is_nan(n)
    is_normal(n)

The following functions accept numbers with no unit:

    log10(n)
    log2(n)
    ln(n)
    sin(n)
    cos(n)
    tan(n)
    asin(n)
    acos(n)
    atan(n)
    acosh(n)
    cosh(n)
    asinh(n)
    sinh(n)
    atanh(n)
    tanh(n)
    ai(n)
    cbrt(n)
    cot(n)
    coth(n)
    csc(n)
    csch(n)
    digamma(n)
    eint(n)
    erf(n)
    erfc(n)
    exp(n)
    exp10(n)
    exp2(n)
    exp_m1(n)
    gamma(n)
    j0(n)
    j1(n)
    li2(n)
    ln_1p(n)
    ln_gamma(n)
    recip(n)
    recip_sqrt(n)
    sec(n)
    sech(n)
    signum(n)
    square(n)
    y0(n)
    y1(n)
    zeta(n)
    atan2(a, b)
    agm(a, b)
    gamma_inc(a, b)
    hypot(a, b)
    positive_diff(a, b)
    remainder(a, b)
    factorial(n)
    root(n, k)
    jn(n, k)
    yn(n, k)

### Temperature functions

    to_fahrenheit(t)
    to_celsius(t)
    to_kelvin(t)

### String functions

    strpart(s: string, start: int, length: int)

### Functions about types

    is_number(v)
    is_int(v)
    is_string(v)
    is_boolean(v)
    is_function(v)
    is_void(v)
    is_array(v)
    int_to_num(v)
    num_to_int(v)
    type(v)
    whole_type(v)
