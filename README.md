# lola

`lola` is a scripting language I wrote in order to learn Rust more deeply.

This repository is the core library, without any embeded binary.  
See the [lola-cli](https://gitlab.com/yolenoyer/lola-cli) repository for a more interactive
experience.

# Features of the language

 - Native syntax/calculation for numbers with physical quantities/units
    - Supports most of the SI units like meters, seconds, etc...
    - Example of valid native numbers : `78m`, `112.2s`, `10N`, `10 m.s-2`, `10 m/s2`, `20 kg.m.s−2` ...
 - Arbitrary float number precision (thanks to the wonderful [rug](https://docs.rs/rug/1.9.0/rug)
   crate)
 - Handle utf-8 identifiers
 - Exceptions
 - `//`-like comments
 - Variable scopes
 - Variables can have 7 different types:
    - `number`   (with sub-types representing a physical quantity like *length*, *time*, *mass*...)
    - `int`      (system-native 64bits integers)
    - `string`
    - `boolean`  (true or false)
    - `function` (anything that can be called)
    - `array`    (a collection of several values)
    - `void`
 - Operators
    - arithmetic (`+`, `-`, `*`, `/`, `^`, `%`)
    - assignment (`=`, `+=`, `*=`, `++`, ...)
    - comparison (`==`, `!=`, `<`, `<=`, `>`, `>=`)
    - boolean logic (`&&`, `||`, `!`)
    - parentheses override natural precedence
 - Unit conversion operators:
    - `~` : normal conversion (e.g. : `100m + 1km ~mm` returns `1100000mm`)
    - `~!` : raw conversion (e.g. : `100m ~!km` returns `100km`, or `100m ~!` returns `100` without
      unit)
 - Arrays : `areas = [ 2km2, 78m2, 4m2 ]`, indexing with square brackets : `areas[0]`
 - Conditionals, loops, flow control:
    - `if/else`
    - `loop` : endless loop, breakable with `break`
    - `while x < 144 { ... }`
    - `for wheel in car_wheels { ... }`
    - `break`
    - `continue`
    - `return`
    - `leave` : ends the 1st-encountered curly-bracket block
 - Many language constructs are normal expressions, with the last expression (without ending semi-colon)
   as the block result
 - Many, many native mathematical functions (bindings to the wonderful
   [rug](https://docs.rs/rug/1.9.0/rug/struct.Float.html) crate functions)
 - User-defined functions : `fn square(n) n ^ 2`
 - Anonymous functions: `square = fn(n) n ^ 2` (functions are regular variables)
 - Function parameters can be typed (an error will occur when types are not respected):
    - `fn speed(distance: number, time_spent: number) { distance / time_spent }`
    - `fn show_msg(message: string) { print("The message is:", message) }`
 - Number parameters can be specialized by quantity (the 2 examples below are equivalent):
    - `fn speed(distance: number(length), time_spent: number(time)) { distance / time_spent }`
    - `fn speed(distance: {length},       time_spent: {time})       { distance / time_spent }`

# Major lacks and caveats / TODOs

 - Function recursion is not supported, and would be hard to implement;
 - Several operations imply much more data-cloning than it should do;
 - Does not handle `/* ... */` block comments;

# Lola code examples

## Displaying the time that light takes to go from the Sun to the Earth:
```
// Returns the time that light needs to travel the given distance.
fn light_travel_time(length: {length}) {
    let c = 1 ly/y; // Speed of light
    let time = length / c;
    to_readable_time(time)
}

// Converts the given time to a readable value, depending on its range.
fn to_readable_time(time: {time}) {
    if      time > 1y    time ~y
    else if time > 1d    time ~d
    else if time > 1h    time ~h
    else if time > 1min  time ~min
    else if time > 1s    time ~s
    else if time > 1ms   time ~ms
    else if time > 1µs   time ~µs
    else                 time ~ns
}

sun_to_earth_distance = 1 au; // Astronomical units
print(
    "The light takes",
    light_travel_time(sun_to_earth_distance),
    "to travel from the sun to the earth"
)
```
### Output:

```
The light takes 8.316746397269273531891185868324946 min to travel from the sun to the earth
```

## A yield pattern implementation:

This demo uses a *yield* pattern to display each element of an array.  
The last print, `(void)`, shows that the array has been fully yielded.

```
// Returns a function which will return a number, each time incremented by one.
fn counter(start: {}, stop: {}) {
    let i = start;
    fn() {
        if i >= stop {}
        else i++
    }
};

// Returns a function which will return each element of the given array (one element per call).
fn array_yielder(arr: array) {
    let iter = counter(0, len(arr));
    fn() {
        let i = iter();
        if is_void(i) {}
        else arr[i]
    }
};

let a = [ "one", "two", "three", "bye!" ];

print("Array content:", a);
print("");

let yield = array_yielder(a);
let i = 5;
while i-- > 0 {
    print(yield());
}
```

### Output:

```
Array content: [ one, two, three, bye! ]

one
two
three
bye!
(void)
```

## Generating the Fibonacci suite, in two versions (1st = Readable, 2nd = Geek version):

```
// Returns an array containing the 'n' first terms of the Fibonacci suite.
fn fibo1(n) {
    let terms = [];
    let a = 1;
    let b = 1;
    while n-- > 0 {
        terms += a;
        let next = a + b;
        a = b;
        b = next;
    };
    terms
};

// The most possible condensed version of the Fibonacci suite function,
// revealing some nice features of the language.
// It's unreadable, but it's working.
fn fibo2(n) {
    let terms = [];
    let a = 0;
    let b = 1;
    while (b = a + (a = b); n-- > 0) terms += a
};

print("Version 1:", fibo1(6));
print("Version 2:", fibo2(6));
```

### Output:

```
Version 1: [ 1, 1, 2, 3, 5, 8 ]
Version 2: [ 1, 1, 2, 3, 5, 8 ]
```

## Using exceptions

```
fn square_root(n: number) {
    if (n~! < 0) {
        throw "Unable to compute the square root of a negative number";
    };
    sqrt(n)
};

fn good_stuff() {
    print(square_root(4km2));
}

fn bad_stuff() {
    print(square_root(-2m2));
}


print("### STEP 1: Nothing to catch, everything ok:");
try {
    good_stuff();
}
catch {
    print("This messsage will not be displayed")
};
print("");


print("### STEP 2: Catch without exception variable:");
try {
    bad_stuff();
}
catch {
    print("Something was catched, but I don't know what!")
};
print("");


print("### STEP 3: Catch with exception variable:");
try {
    bad_stuff();
}
catch err {
    print("Something was catched:", err)
};
print("");


print("### STEP 4: Uncatched exception:");
bad_stuff();


print("### STEP 5: This will never be shown, because the last exception had not been catched");
```

### Output:

```
### STEP 1: Nothing to catch, everything ok:
2 km

### STEP 2: Catch without exception variable:
Something was catched, but I don't know what!

### STEP 3: Catch with exception variable:
Something was catched: Unable to compute the square root of a negative number

### STEP 4: Uncatched exception:
Exception: Unable to compute the square root of a negative number
```
